
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University::Frequent Ask Questions</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>   <!--Menu Area -->
    <div id="nav">
             <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>
              <p align="center">Frequent Ask Questions (FAQS) </p>
            </h2>
            <ul>
              <li>
                <div class="desc1">
                
<p align="justify"><b><font color="#0000CC">Q. Is Southern University Bangladesh  accredited?</font></b><br />
A. In Bangladesh there is no official accredited body yet, but University Grants Commission (UGC) is the highest controlling authority to oversee all activities about Government and non Government University.<br />

Q. <b><font color="#0000CC">Is Southern University Bangladesh approved from Ministry of Education?</font></b><br />
Yes, in the year 2003 Ministry of Education approved Southern University and UGC approved its courses.<br />

Q. <b><font color="#0000CC">Is Southern University authorized to issue degrees?</font></b><br />
A. Southern University is officially approved by the Ministry of Education of Bangladesh and is authorized to grant degrees at Bachelor and Masters levels.<br />


Q.<b><font color="#0000CC"> When was Southern University established? </font></b><br />
A. Southern University was established in 1998 , as an affiliated institute of UK, USA . In 2003 got approval from Ministry of Education as Non Government University in Chittagon Bangladesh.<br />
Q.<b><font color="#0000CC"> Are Southern University degrees recognized in other countries or by other universities?</font></b><br />
A. Employers in most countries around the world recognize  Southern University degrees. However, foreign governments are under no obligation to declare recognition of  Southern University degrees. Each university has its own policy concerning the recognition of credits earned at other universities. To determine recognition by other universities,students should contact the other schools regarding their specific policy concerning acceptance of credits earned at Southern University.<br />
Q. <b><font color="#0000CC">Does Southern offer distance learning programs and on-campus programs?</font></b><br />
A. Southern University  do not offers distance education programs, we offers only on-campus programs.<br /><br />
Q.<b><font color="#0000CC"> Does Southern University accept credits earned at other universities? If so, how many?</font></b><br />
A. Yes, it is possible to transfer credits earned at other recognized educational institutions. Only courses in which
you have earned at least a grade of “C” grade or better will be accepted. The number of credits accepted varies with the student’s degree program.<br />

Q.<b><font color="#0000CC"> What type of testing must I take to be accepted for admission to Southern University?</font></b><br />
A. Students whose spoken and written language is not English must score above 500 on TOEFL or 5.5 on IELTS prior to admission to Southern University programs .<br />

Q.<b><font color="#0000CC"> Does Southern University offer financial assistance to students?</font></b><br />
A. Southern University offers students convenient monthly installment plans to match their budget. Students who pay in full at the beginning of their program receive a tuition discount.  Financial aid is available from the university subject to students academic performance and financial status .<br />

Q.<b><font color="#0000CC">Does Southern University issue academic transcripts?</font></b><br />
A. Yes, Southern issues official academic transcripts after the student successfully completes all his or her courses. Records of academic progress are also kept, and interim transcripts may be issued to the student following completion of course work for any semester.<br />

Q.<b><font color="#0000CC"> Who are the faculty of Southern University?</font></b><br />
A. The university faculty consists of a wide variety of adjunct professors from reputed universities as well as Southern’s own fulltime professors. All professors have established credentials as teachers and have been selected for their range of both practical and academic experience.<br />

Q. <b><font color="#0000CC">How do the students pay their application and tuition fees?</font></b><br />
A. Fees can be paid by check, money order, credit card or wire transfer. 
<br />
Q.<b><font color="#0000CC"> Can I earn more than one degree from Southern University?</font></b><br />
A. Certainly. In fact, a student could begin by earning the Bachelors and Masters degrees to reach his or her educational goals. Southern exists to help you reach your full potential and achieve your academic goals. We pledge to make your educational experience as efficient and rewarding as possible.<br />

Q.<b><font color="#0000CC"> What is the language of instruction at Southern University?</font></b><br />
A. Programs are generally delivered in English using English language textbooks. <br />

Q.<b><font color="#0000CC"> Do students need to send official transcripts and diplomas earned at other educational institutions?</font></b><br />
A. Yes. All applicants for admission to Southern should provide  official transcripts, translated into English and notarized or attested by an appropriate government official. The student must submit copies of all diplomas they have earned. These documents enable the Academic Committee to approve enrollment in programs and to assign transfer credit for previous academic achievements. Each degree program requires different documentation; we have included an Applicant Check list to help you in submitting all the correct documents with your Application for Admission. To prevent delays in processing your application, be sure to submit all required documents in one complete package.<br /><br />

Q.<b><font color="#0000CC"> How do students communicate with faculty and staff at Southern University?</font></b><br />
A. Faculty and staff are available to assist students with their programs, advise about courses and curriculum and to help the students succeed. Communication may be accomplished in a number of ways.<br /><br /></p>

<br />
<br/>

<b><font color="#330066"><marquee scrolldelay="350"><p align="center">Committed to Academic Excellence</p></marquee></font></b> <br />


                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>