
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Department of Civil Engineering</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>   <!--Menu Area -->
    <div id="nav">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of Civil Engineering</h2>
            <ul>
              <li>
                <div class="desc1">
<h3>FACULTY OF SCIENCE & ENGINEERING</h3></br></br>
Department of Civil Engineering</br>
<b>Bachelor of Science in Civil Engineering</b></br>
General Education, Core & Elective Courses: 161 semester credit hours</br></br>

<p align="justify">Bachelor of Science in Civil Engineering (B.Sc in Civil Engineering) degree is 4 years program, requires 161 credit hours of Post Secondary college level study providing quality education at undergraduate levels. This requires Semester credit hours depending upon the academic background. Course work features practices as which enable students to work effectively in business and education environment.</p></br></br>

<b>Core Courses: 42 Courses</b></br>
<b>Optional Courses: 2 courses</b></br>
<b>Project 1 project</b></br>
<b>Total = 161 Credit Hours</b></br></br>
 
EN 101 English</br>
EN 203 Advance English</br>
CH 101 Chemistry-I</br>
CH 103 Chemistry-II</br>
CH 102 Chemistry Lab</br>
PH 101 Physics</br>
PH 102 Physics Lab</br>
MA 101 Engineering Mathematics-I</br>
SO 101 Economics & Sociology</br>
MA 103 Engineering Mathematics-II</br>
CS 100 Computer Skills</br>
MA 203 Engineering Mathematics-III</br>
MA 301 Engineering Mathematics-IV</br>
CS 201 Computer Programming & Numerical Methods</br>
CS 202 Computer Application Lab</br>
AC 203 Accounting & Costing </br></br>

<b>Core Courses</b></br></br>

EE 121 Basic Electrical Engineering</br>
EE 122 Basic Electrical Engineering Lab</br>
CE 101 Engineering Mechanics-I</br>
CE 100 Civil Engineering Drawing-I</br>
CE 102 Civil Engineering Drawing-II</br>
CE 105 Surveying</br>
CE 106 Practical Surveying</br>
CE 103 Engineering Mechanics-II</br>
CE 201 Mechanics of Materials-I</br>
CE 202 Mechanics of Materials Lab</br>
CE 203 Engineering Material</br>
CE 204 Engineering Material Lab</br>
CE 211 Mechanics of Materials-II</br>
CE 231 Engineering Geology & Geomorphology</br>
CE 261 Fluid Mechanics</br>
CE 262 Fluid Mechanics Lab</br>
CE 200 Details of Construction & Estimating</br>
CE 311 Structural Analysis & Design-I</br>
CE 315 Design of Concrete Structures</br>
CE 331 Geotechnical Engineering-I</br>
CE 351 Environmental Engineering-I</br>
CE 312 Structural Analysis & Design Lab-I</br>
CE 332 Geotechnical Engineering Lab</br>
CE 352 Environmental Engineering Lab-I</br>
CE 313 Structural Analysis & Design-II</br>
CE 317 Design of Concrete Structures II</br>
CE 361 Open Channel Flow</br>
CE 341 Transportation Engineering</br>
CE 333 Geotechnical Engineering</br>
CE 318 Design of Concrete Structures Lab -I</br>
CE 342 Transportation Engineering Lab -I</br>
CE 362 Open Channel Flow Lab</br>
CE 461 Hydrology & Irrigation</br>
CE 411 Structural Analysis & Design-III</br>
CE 451 Environmental Engineering II</br>
CE 403 Project planning & Management</br>
CE 441 Transportation Engineering-II</br>
CE 412 Structural Analysis & Design Lab-II</br>
CE 442 Transportation Engineering Lab-II</br>
CE 415 Prestressed Concrete</br>
CE 431 Foundation Engineering</br>
CE 418 Design of Concrete Structures Lab-II</br>
CE 452 Environment Engineering Lab-II</br></br>

<b>Optional Courses</b></br>
Option - 1</br></br>

CE 419 Introduction to Finite Element Method</br>
CE 421 Structural Dynamics & Earthquake Engineering</br></br>

Option - 2</br></br>

CE 453 Environmental Engineering III(Solid Waste Management)</br>
CE 455 Environmental Engineering IV(Environmental Pollution Control)</br></br>

Option - 3</br></br>

CE 463 Flood Control & River Engineering</br>
CE 471 Hydraulic Structures</br></br>

Option - 4</br></br>

CE 443 Transportation Engineering -III(Traffic Planning and Management)</br>
CE 445 Transportation Engineering IV(Highway Drainage and Airport Engineering)</br>
CE 400 Project /Dissertation</br>
                </div>
              </li>
             </ul>
          </div>
  
        </div>
           <br class="clear" />
        </div>
      </div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>