
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Department of English</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>   <!--Menu Area -->
    <div id="nav">
       <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   

    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of English</h2>
            <ul>
              <li>
                <div class="desc1">
<h3>FACULTY OF ARTS, LAW AND SOCIAL SCIENCE</h3></br></br>
Departments of English</br>
<b>Bachelor of Arts in English (B.A. in English) </b></br>
General Education Core & Elective Courses: 120 semester credit hours</br></br>

<p align="justify">The Bachelor of Arts in English (BA in English) degree is 4 years program, requires 120 semester hours of Post Secondary college-level study providing quality education at undergraduate levels. This requires Semester credit hours depending upon the academic background. Course work features practices, as which will enable students to work effectively in business and education environment.
 </p></br></br>

<b>General Education : 10 Courses X 3 Credit = 30 Credit Hours</b></br>
<b>Core : 24 Courses X 3 Credit = 72 Credit Hours</b></br>
<b>Electives : 4 Courses X 3 Credit = 12 Credit Hours </b></br>
<b>Project : 1 Project =6 Credit Hours </b></br>

<b>General Education</b></br>/br>
1.EN-1010, English Composition</br>
2. EN 1210 Spoken English</br>
3. IR-1010, International Relations</br>
4. SO 1020 Contemporary Sociology</br>
5. SO-1010, Bangladesh Studies</br>
6. BI 1010 Health Science Management</br>
7. ES1010 Environmental Science</br>
8. CS 1000 Computer Fundamentals & Application</br>
9. MA 1020, Introduction to Statistics</br>
<b>Core</b></br></br>
10. EN 2010 English composition Theory, writing and application</br>
11. MG 2010, Introduction to Business</br>
12. EN 2020 Literature</br>
13. EN 2030 Middle English Literature</br>
14. EN 2040 Advance Writing</br>
15. EN 2050 Introduction to Linguistics</br>
16. EN 2060 The Structure of English </br>
17. EN 2070 Translation</br>
18. EN 2080 Writing for Profession</br>
19. EN 2090 The novel</br>
20. EN 3010 Communication Skills</br>
21. EN 3020 Theories of Human Communication</br>
22. EN 3030 Reporting Public Affairs</br>
23. EN 3040 Renaissance comedy: Shakespeare and Jonson</br>
24. EN 3050 American Literature</br>
25. EN 3060 American Fiction</br>
26. EN 3070 New writing in English</br>
27. EN 3080 Shakespeare</br>
28. EN 3090 Poetry Writing</br>
29. EN 4010 Critical Reading and Writing</br>
30. EN 4020 Language Learning</br>
31. EN 4030 Practical Criticism</br>
35. MG 4260, Introduction to Research</br>
36. EN 4040 Methods and Materials for Teaching English</br>
37. EN 4050 Teaching English as a Second Language</br>
38. EN 4060 Classroom Management</br>
39. EN 4070 Educational Psychology</br>
40. EN 4090 Undergraduate Capstone Project Report(6 CR/hrs)</br></br>
<b>Optional </b></br></br>
1. EN 1002 Reading English(Required For Selective Students)</br>
2. SO 1025 Military Science (Optional)</br></br>
<h3>MA in English</h3></br></br>
<b>Course Offering for 2 year</b></br></br>
1. ENG-501 Advanced English Grammar 
2. ENG-506 Rhetoric & Prosody 
3. ENG-510 Victorian Literature
4. ENG-502 English Phonetics of & Phonology 
5. ENG- 508The Romantics 
6. ENG-511 17th Century Prose and Drama
7. ENG-503The Experience of Literature 
8. ENG-509 Elizabethan and Jacobean Drama 
9. ENG-512 Elizabethan and 17`h Century Poetry- 
10. ENG-513 Old and Medieval English literature 
<b>MA in English Course Offering for One Year</b></br></br>
1. ENG-504 Structure of English</br>
2. ENG-505 Elementary Linguistics </br>
3. ENG-515 Modern Poetry</br>
3. ENG-514 William Shakespeare </br>
4. ENG-517 Modern Novel</br>
5. ENG-518 Classics in Translation</br>
6. ENG-507 Literary Criticism </br>
7. ENG-516 Modern Drama </br>
8. ENG-519 Postmodernism</br>
9. ENG-520 Thesis/ Dissertation</br>




                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>
</div></body>
</html>