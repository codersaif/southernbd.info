
<!DOCTYPE html>
<title>SUB | Picture  Gallery</title>
<meta charset="utf-8" />
<link rel="stylesheet" href="styles/gallery.css" type="text/css" />
<script src="scripts/gallery.js" type="text/javascript"></script>
<!--[if IE]><style>#header h1 a:hover{font-size:75px;}</style><![endif]-->
</head>
<body>
<div class="main-container">
  <header>
    <h2><a href="gallery.html"><img src="images/logo.jpg" width="155" height="143"><font color="#0000FF">Southern University Bangladesh</font></a></h2>
  </header>
</div>
<div class="main-container"></div>
<div class="main-container">
  <div class="container1"><br />

        <h1>	<a href="../index.html">Back to Home</a> </h1>
            <div id="gallery" class="box">
      <ul>

        <li><a data-gal="prettyPhoto[gallery2]" href="images/g.jpg" title=""><img src="images/g.jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (1).jpg" title=""><img src="images/g (1).jpg" width="260" height="190" alt="" /></a></li>
        <li class="last"><a data-gal="prettyPhoto[gallery2]" href="images/g (2).jpg" title=""><img src="images/g (2).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (3).jpg" title=""><img src="images/g (3).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (4).jpg" title=""><img src="images/g (4).jpg" width="260" height="190" alt="" /></a></li>
        <li class="last"><a data-gal="prettyPhoto[gallery2]" href="images/g (5).jpg" title=""><img src="images/g (5).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (6).jpg" title=""><img src="images/g (6).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (7).jpg" title=""><img src="images/g (7).jpg" width="260" height="190" alt="" /></a></li>
        <li class="last"><a data-gal="prettyPhoto[gallery2]" href="images/g (13).jpg" title=""><img src="images/g (13).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (8).jpg" title=""><img src="images/g (8).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (9).jpg" title=""><img src="images/g (9).jpg" width="260" height="190" alt="" /></a></li>
        <li class="last"><a data-gal="prettyPhoto[gallery2]" href="images/g (14).jpg" title=""><img src="images/g (14).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (10).jpg" title=""><img src="images/g (10).jpg" width="260" height="190" alt="" /></a></li>
        <li><a data-gal="prettyPhoto[gallery2]" href="images/g (11).jpg" title=""><img src="images/g (11).jpg" width="260" height="190" alt="" /></a></li>
        <li class="last"><a data-gal="prettyPhoto[gallery2]" href="images/g (12).jpg" title=""><img src="images/g (12).jpg" width="260" height="190" alt="" /></a></li>
        
        
      </ul>
      <br class="clear" />
    </div>

<br />
      
      <div class="clear"></div>
    </div>
    
 
 </div>
<div class="main-container">
 </div>
 
<div class="callout"></div> 
 <footer>
  <p class="tagline_left">Copyright &copy; 2013 - All Rights Reserved <strong>-Southern University Bangladesh</strong></p>
    <p class="tagline_right">Design by :: <strong><a href="#01675922506#"> MD AL Amin</a> </strong></p>
    <br class="clear" />
  </footer>

<br />
<br />
</body>
</html>
