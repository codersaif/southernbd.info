
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html><head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-15">
	<title>New Student Application</title>

	<script language="JavaScript" src="New%20Student%20Application_files/validationcheck.js"></script>
	<link rel="stylesheet" href="New%20Student%20Application_files/public_page_style.css" type="text/css">

	</head>

	
	<body leftmargin="5" topmargin="5" onresize="if (navigator.family == 'nn4') window.location.reload()" onload="document.frmaddstudent.firstname.focus()" alink="#CC0000" bgcolor="#FFFFFF" link="#CC0000" marginheight="0" marginwidth="0" text="#000000" vlink="#CC0000">
	


	<center>
	
		
		<div class="schoolname">Southern University Bangladesh </div> 
		 <div style="font-family: times;"> GEC/Halishahar,Chittagong </div>
	
	
	

	     <div align="center">
	<br>
	
	<div class="basic bold_color header">
		New Student Application
	</div>
	

	

	<form enctype="multipart/form-data" method="post" name="frmaddstudent" action="public_save_student_registration.jsp">
	<input name="id" value="30" type="hidden">
	<input name="studentid" value="0" type="hidden">
	<input name="picturefileid" value="0" type="hidden">
	<input name="username" value="" type="hidden">
	<input name="password" value="" type="hidden">
	<input name="redirpage" value="" type="hidden">
	<input name="languageid" value="12" type="hidden">
	<input name="color" value="" type="hidden">
	<input name="continuelater" value="no" type="hidden">
	<input name="previouspage" value="public_student_registration" type="hidden">


	<br>
	
	<div class="basic bold_color sub_header">
		General Information
	</div>
	
	<div class="basic section_description">
	<p>
		
	</p>
	</div>

	<table>

	<tbody><tr><td class="right-align bold" width="30%">Student Full Name</td><td align="left"><input name="XP1189" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Blood Group</td><td align="left"><input name="XP1255" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr>


	<tr>
		<td class="right-align bold" width="30%"> First Name </td>
		<td>
			<input name="firstname" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span>
		</td>
	</tr>

	<tr><td class="right-align bold" width="30%">Place of Birth</td><td align="left"><input name="XP1196" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr>


	<tr>
		<td class="right-align bold">
			Middle Name
		</td>
		<td>
			<input name="middlename" class="longtext" type="text">
		</td>
	</tr>

	

	<tr>
		<td class="right-align bold">
			Last name
		</td>
		<td>
			<input name="lastname" class="longtext" type="text">&nbsp;<span class="required">  (Required) </span>
		</td>
	</tr>
	
	

	
	<tr>
	<td class="right-align bold" width="30%">
			Picture File
	</td>
	<td>
		
		<input name="picturefile" type="file">
		
			<span class="required">  (Required) </span>
		
		</td>
	</tr>
	


	<tr>
	<td class="right-align bold" width="30%">Program</td>
	<td>
	<select name="program">
	
                     <option selected="selected" value="185,417,0">BUS: Bachelor of Business Administration (BBA)</option>
					
                     <option value="185,443,0">BUS: Executive Master of Business Administration (EMBA)</option>
					
                     <option value="185,441,0">BUS: Master of Business Administration (MBA(2YRS))</option>
					
                     <option value="185,425,0">BUS: Masters of Business Administration (MBA(1YR))</option>
					
                     <option value="190,434,0">CSIT: Bachelor of Sc. in Computer Sc &amp; IT (BSCSIT(2YRS))</option>
					
                     <option value="190,433,0">CSIT: Bachelor of Sc. in Computer Sc. &amp; IT (BSCSIT(4YRS))</option>
					
                     <option value="190,435,0">CSIT: Master of Sc in Computer Sc. &amp; IT (MSCSIT(2YRS))</option>
					
                     <option value="190,436,0">CSIT: Master of Sc. in Computer Sc. &amp; IT (MSCSIT(1YR))</option>
					
                     <option value="184,426,0">LAW: Bachelor of Laws (LLB(2YRS))</option>
					
                     <option value="184,416,0">LAW: Bachelor of Laws (LLB(4YRS))</option>
					
                     <option value="184,429,0">LAW: Master of Law (LLM(1YR))</option>
					
                     <option value="184,430,0">LAW: Master of Law (LLM(2YRS))</option>
					
                     <option value="187,446,0">ENG: Bachelor of Arts in English (BA-ENGLISH)</option>
					
                     <option value="187,447,0">ENG: Master of Arts  in English (MA-ENGLISH(1YR))</option>
					
                     <option value="187,428,0">ENG: Master of Arts in English (MA-ENGLISH(2YRS))</option>
					
                     <option value="199,448,0">PHRM: Bachelor of Pharmacy (BPHARM)</option>
					
                     <option value="199,450,0">PHRM: Master of Pharmacy (MPHARM)</option>
					
                     <option value="199,449,0">PHRM: Master of Pharmacy (MPHARM(RESEARCH))</option>
					
                     <option value="191,437,0">EEE/ECE: Bachelor of  Sc. in Electrical &amp; Electronic E... (EEE(2YRS))</option>
					
                     <option value="191,439,0">EEE/ECE: Bachelor of Sc. in  Electronic &amp; Communicatio... (ECE(2YRS))</option>
					
                     <option value="191,438,0">EEE/ECE: Bachelor of Sc. in Electrical &amp; Electronic En... (EEE(4YRS))</option>
					
							<option value="191,438,274">EEE/ECE: Bachelor of Sc. in Electrical &amp; Electronic En... (EEE(4YRS) - EEE)</option>
							
                     <option value="191,440,0">EEE/ECE: Bachelor of Sc. in Electronic &amp; Communication... (ECE(4YRS))</option>
					
                     <option value="188,431,0">CIVIL: Bachelor of Sc. in Civil Engineering (BSC-CIVIL)</option>
					
                     <option value="188,432,0">CIVIL: Bachelor of Sc. in Civil Engineering (BSC-CIVIL(3YRS))</option>
					
                     <option value="215,452,0">IS: M.A in Islamic Studies (MA-ISLAMIC STUDIES)</option>
					
                     <option value="215,453,0">IS: M.A in Islamic Studies (MA-ISLAMIC STUDIES (2YEARS))</option>
					
                     <option value="217,454,0">HTM: Bachelor of Business in HTM (HTM)</option>
					
	</select>
	</td>
	</tr>
	
	
	
	<tr>
	<td class="right-align bold" width="30%">
			Semester interested in enrolling for
	</td>
	<td align="left">
	<select name="enrollmentsemesterid">
	
	</select>
	</td>
	</tr>
	

	

	
	<tr>
		<td class="right-align bold" width="30%">Gender</td>
		<td>
		<input name="sex" value="1" type="radio">Male
		<input name="sex" value="2" checked="checked" type="radio">Female
		</td>
	</tr>

	


	

	
	<tr>
		<td class="right-align bold" width="30%">Home Phone</td>
		<td>
		<input name="homephone" class="longtext" type="text">	
		
		&nbsp;<span class="required">  (Required) </span>
		
		</td>
	</tr>
	

	

	
	<tr>
		<td class="right-align bold">
			Cell Phone
		</td>
		<td>
			<input name="cellphone" class="longtext" type="text">
			<input name="smssupported" value="checked" type="hidden">
		</td>
	</tr>
	

	
	<tr>
		<td class="right-align bold" width="30%">Email</td>
		<td><input name="email" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
		
		</td>
	</tr>
	

	

	
		<tr>
		<td class="right-align bold" width="30%">Religion</td>
		<td colspan="2" align="left">
		<select name="faith">
		<option selected="selected" value="0">.....</option>
		
			<option value="1">Islam</option>
			
			<option value="2">Christan</option>
			
			<option value="3">Bodho</option>
			
			<option value="4">Hindu</option>
			
			<option value="5">Other</option>
			
		</select>
		
		
		
		</td>
		</tr>

		

	

	

		<tr>
		<td class="right-align bold" width="30%">Race/Ethnic Origin</td>
		<td colspan="2" align="left">
		<select name="race">
		<option selected="selected" value="0">.....</option>
		
		</select>
		
		
		
		</td>
		</tr>

		

	

	<!-- PREFERRED LANGUAGE -->

	

	<!-- BATCH NUMBER -->

	

	

	

	
	<tr>
	<td class="right-align bold" width="30%"> Birth Date</td>
	<td><input name="dob" id="dob" class="shorttext" onblur="dateCheck(this.name,this.value,'d/M/yyyy');" type="text">
	
	&nbsp;<span class="required">  (Required) </span>
	
	<font face="Arial" size="1">d/M/yyyy</font>
	</td>
	</tr>
	

	

	<!-- DEPOSIT REFUND DATE -->

	

	<!-- COLUMN 2 STARTS -->

	<tr><td class="right-align bold" width="30%">Work Phone</td><td align="left"><input name="XP1194" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">FAX</td><td align="left"><input name="XP1195" class="shorttext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr>

	
	<tr>
	<td class="right-align bold" width="30%">
		Address</td>
	<td align="left">
	<input name="address" class="longtext" type="text">
	
	</td>
	</tr>
	

	


	
	<tr>
	<td class="right-align bold">
	City
	</td>
	<td>
	<input name="city" class="longtext" type="text">
	
	</td>
	</tr>
	

	
	

		
	<tr>
	<td class="right-align bold">
		State/Province
	</td>
	<td align="left">
	<select name="state">
	<option selected="selected" value="">--Select--</option>
		
		<option value="AL">Alabama</option>
		
		<option value="AK">Alaska</option>
		
		<option value="AB">Alberta</option>
		
		<option value="AS">American Samoa</option>
		
		<option value="AZ">Arizona</option>
		
		<option value="AR">Arkansas</option>
		
		<option value="AE">Armed Forces</option>
		
		<option value="AA">Armed Forces Americas</option>
		
		<option value="AP">Armed Forces Pacific</option>
		
		<option value="BC">British Columbia</option>
		
		<option value="CA">California</option>
		
		<option value="CO">Colorado</option>
		
		<option value="CT">Connecticut</option>
		
		<option value="DE">Delaware</option>
		
		<option value="DC">District of Columbia</option>
		
		<option value="FM">Federated States of Micronesia</option>
		
		<option value="FL">Florida</option>
		
		<option value="GA">Georgia</option>
		
		<option value="GU">Guam</option>
		
		<option value="HI">Hawaii</option>
		
		<option value="ID">Idaho</option>
		
		<option value="IL">Illinois</option>
		
		<option value="IN">Indiana</option>
		
		<option value="IA">Iowa</option>
		
		<option value="KS">Kansas</option>
		
		<option value="KY">Kentucky</option>
		
		<option value="LA">Louisiana</option>
		
		<option value="ME">Maine</option>
		
		<option value="MB">Manitoba</option>
		
		<option value="MH">Marshall Islands</option>
		
		<option value="MD">Maryland</option>
		
		<option value="MA">Massachusetts</option>
		
		<option value="MI">Michigan</option>
		
		<option value="MN">Minnesota</option>
		
		<option value="MS">Mississippi</option>
		
		<option value="MO">Missouri</option>
		
		<option value="MT">Montana</option>
		
		<option value="NE">Nebraska</option>
		
		<option value="NV">Nevada</option>
		
		<option value="NB">New Brunswick</option>
		
		<option value="NH">New Hampshire</option>
		
		<option value="NJ">New Jersey</option>
		
		<option value="NM">New Mexico</option>
		
		<option value="NY">New York</option>
		
		<option value="NF">Newfoundland</option>
		
		<option value="NC">North Carolina</option>
		
		<option value="ND">North Dakota</option>
		
		<option value="MP">Northern Mariana Islands</option>
		
		<option value="NT">Northwest Territories</option>
		
		<option value="NS">Nova Scotia</option>
		
		<option value="OH">Ohio</option>
		
		<option value="OK">Oklahoma</option>
		
		<option value="ON">Ontario</option>
		
		<option value="OR">Oregon</option>
		
		<option value="PW">Palau</option>
		
		<option value="PA">Pennsylvania</option>
		
		<option value="PE">Prince Edward Island</option>
		
		<option value="PR">Puerto Rico</option>
		
		<option value="QC">Quebec</option>
		
		<option value="RI">Rhode Island</option>
		
		<option value="SK">Saskatchewan</option>
		
		<option value="SC">South Carolina</option>
		
		<option value="SD">South Dakota</option>
		
		<option value="TN">Tennessee</option>
		
		<option value="TX">Texas</option>
		
		<option value="UT">Utah</option>
		
		<option value="VT">Vermont</option>
		
		<option value="VI">Virgin Islands</option>
		
		<option value="VA">Virginia</option>
		
		<option value="WA">Washington</option>
		
		<option value="WV">West Virginia</option>
		
		<option value="WI">Wisconsin</option>
		
		<option value="WY">Wyoming</option>
		
		<option value="YT">Yukon</option>
		

	</select>

	
	</td>
	</tr>
	

	


		
	<tr>
	<td class="right-align bold">
	Zip/Postcode
	</td>
	<td>
	<input name="zip" class="longtext" type="text">
	
	</td>
	</tr>
	

	


		
	<tr>
	<td class="right-align bold">
		Country
	</td>
	<td align="left">
	<select name="country">
		
		<option selected="selected" value="AL">ALBANIA</option>
		
		<option value="DZ">ALGERIA</option>
		
		<option value="AS">AMERICAN SAMOA</option>
		
		<option value="AD">ANDORRA</option>
		
		<option value="AI">ANGUILLA</option>
		
		<option value="AG">ANTIGUA AND BARBUDA</option>
		
		<option value="AR">ARGENTINA</option>
		
		<option value="AM">ARMENIA</option>
		
		<option value="AW">ARUBA</option>
		
		<option value="AU">AUSTRALIA</option>
		
		<option value="AT">AUSTRIA</option>
		
		<option value="AZ">AZERBAIJAN</option>
		
		<option value="BS">BAHAMAS</option>
		
		<option value="BH">BAHRAIN</option>
		
		<option value="BD">BANGLADESH</option>
		
		<option value="BB">BARBADOS</option>
		
		<option value="BY">BELARUS</option>
		
		<option value="BE">BELGIUM</option>
		
		<option value="BZ">BELIZE</option>
		
		<option value="BJ">BENIN</option>
		
		<option value="BM">BERMUDA</option>
		
		<option value="BO">BOLIVIA</option>
		
		<option value="BA">BOSNIA AND HERZEGOVINA</option>
		
		<option value="BW">BOTSWANA</option>
		
		<option value="BR">BRAZIL</option>
		
		<option value="VG">BRITISH VIRGIN ISLANDS</option>
		
		<option value="BN">BRUNEI</option>
		
		<option value="BG">BULGARIA</option>
		
		<option value="BF">BURKINA FASO</option>
		
		<option value="KH">CAMBODIA</option>
		
		<option value="CM">CAMEROON</option>
		
		<option value="CA">CANADA</option>
		
		<option value="CV">CAPE VERDE</option>
		
		<option value="KY">CAYMAN ISLANDS</option>
		
		<option value="CL">CHILE</option>
		
		<option value="CN">CHINA</option>
		
		<option value="CO">COLOMBIA</option>
		
		<option value="CK">COOK ISLANDS</option>
		
		<option value="HR">CROATIA</option>
		
		<option value="CY">CYPRUS</option>
		
		<option value="CZ">CZECH REPUBLIC</option>
		
		<option value="DK">DENMARK</option>
		
		<option value="DJ">DJIBOUTI</option>
		
		<option value="DM">DOMINICA</option>
		
		<option value="DO">DOMINICAN REPUBLIC</option>
		
		<option value="TP">EAST TIMOR</option>
		
		<option value="EG">EGYPT</option>
		
		<option value="SV">EL SALVADOR</option>
		
		<option value="EE">ESTONIA</option>
		
		<option value="FJ">FIJI</option>
		
		<option value="FI">FINLAND</option>
		
		<option value="FR">FRANCE</option>
		
		<option value="GF">FRENCH GUIANA</option>
		
		<option value="PF">FRENCH POLYNESIA</option>
		
		<option value="GA">GABON</option>
		
		<option value="GE">GEORGIA</option>
		
		<option value="DE">GERMANY</option>
		
		<option value="GH">GHANA</option>
		
		<option value="GI">GIBRALTAR</option>
		
		<option value="GR">GREECE</option>
		
		<option value="GD">GRENADA</option>
		
		<option value="GP">GUADELOUPE</option>
		
		<option value="GU">GUAM</option>
		
		<option value="GT">GUATEMALA</option>
		
		<option value="GN">GUINEA</option>
		
		<option value="GY">GUYANA</option>
		
		<option value="HT">HAITI</option>
		
		<option value="HN">HONDURAS</option>
		
		<option value="HK">HONG KONG</option>
		
		<option value="HU">HUNGARY</option>
		
		<option value="IS">ICELAND</option>
		
		<option value="IN">INDIA</option>
		
		<option value="ID">INDONESIA</option>
		
		<option value="IE">IRELAND</option>
		
		<option value="IL">ISRAEL</option>
		
		<option value="IT">ITALY</option>
		
		<option value="CI">IVORY COAST</option>
		
		<option value="JM">JAMAICA</option>
		
		<option value="JP">JAPAN</option>
		
		<option value="JO">JORDAN</option>
		
		<option value="KZ">KAZAKHSTAN</option>
		
		<option value="KE">KENYA</option>
		
		<option value="KW">KUWAIT</option>
		
		<option value="LA">LAO PEOPLE'S DEMOCRATIC REPUBLIC</option>
		
		<option value="LV">LATVIA</option>
		
		<option value="LB">LEBANON</option>
		
		<option value="LS">LESOTHO</option>
		
		<option value="LT">LITHUANIA</option>
		
		<option value="LU">LUXEMBOURG</option>
		
		<option value="MO">MACAO</option>
		
		<option value="MK">MACEDONIA</option>
		
		<option value="MG">MADAGASCAR</option>
		
		<option value="MY">MALAYSIA</option>
		
		<option value="MV">MALDIVES</option>
		
		<option value="ML">MALI</option>
		
		<option value="MT">MALTA</option>
		
		<option value="MH">MARSHALL ISLANDS</option>
		
		<option value="MQ">MARTINIQUE</option>
		
		<option value="MU">MAURITIUS</option>
		
		<option value="MX">MEXICO</option>
		
		<option value="FM">MICRONESIA, FEDERATED STATES OF</option>
		
		<option value="MD">MOLDOVA</option>
		
		<option value="MN">MONGOLIA</option>
		
		<option value="MS">MONTSERRAT</option>
		
		<option value="MA">MOROCCO</option>
		
		<option value="MZ">MOZAMBIQUE</option>
		
		<option value="NA">NAMIBIA</option>
		
		<option value="NP">NEPAL</option>
		
		<option value="NL">NETHERLANDS</option>
		
		<option value="AN">NETHERLANDS ANTILLES</option>
		
		<option value="NZ">NEW ZEALAND</option>
		
		<option value="NI">NICARAGUA</option>
		
		<option value="MP">NORTHERN MARIANA ISLANDS</option>
		
		<option value="NO">NORWAY</option>
		
		<option value="OM">OMAN</option>
		
		<option value="PK">PAKISTAN</option>
		
		<option value="PW">PALAU</option>
		
		<option value="PS">PALESTINE</option>
		
		<option value="PA">PANAMA</option>
		
		<option value="PG">PAPUA NEW GUINEA</option>
		
		<option value="PY">PARAGUAY</option>
		
		<option value="PE">PERU</option>
		
		<option value="PH">PHILIPPINES, REPUBLIC OF</option>
		
		<option value="PL">POLAND</option>
		
		<option value="PT">PORTUGAL</option>
		
		<option value="PR">PUERTO RICO</option>
		
		<option value="QA">QATAR</option>
		
		<option value="RO">ROMANIA</option>
		
		<option value="RU">RUSSIAN FEDERATION</option>
		
		<option value="RW">RWANDA</option>
		
		<option value="KN">SAINT KITTS AND NEVIS</option>
		
		<option value="LC">SAINT LUCIA</option>
		
		<option value="VC">SAINT VINCENT AND THE GRENDINES</option>
		
		<option value="WS">SAMOA</option>
		
		<option value="SA">SAUDI ARABIA</option>
		
		<option value="CS">SERBIA AND MONTENEGRO</option>
		
		<option value="SC">SEYCHELLES</option>
		
		<option value="SG">SINGAPORE</option>
		
		<option value="SK">SLOVAKIA</option>
		
		<option value="SI">SLOVENIA</option>
		
		<option value="SB">SOLOMON ISLANDS</option>
		
		<option value="ZA">SOUTH AFRICA</option>
		
		<option value="KR">SOUTH KOREA</option>
		
		<option value="ES">SPAIN</option>
		
		<option value="LK">SRI LANKA</option>
		
		<option value="SZ">SWAZILAND</option>
		
		<option value="SE">SWEDEN</option>
		
		<option value="CH">SWITZERLAND</option>
		
		<option value="TW">TAIWAN</option>
		
		<option value="TZ">TANZANIA, UNITED REPUBLIC OF</option>
		
		<option value="TH">THAILAND</option>
		
		<option value="TG">TOGO</option>
		
		<option value="TO">TONGA</option>
		
		<option value="TT">TRINIDAD AND TOBAGO</option>
		
		<option value="TN">TUNISIA</option>
		
		<option value="TR">TURKEY</option>
		
		<option value="TM">TURKMENISTAN</option>
		
		<option value="TC">TURKS AND CAICOS ISLANDS</option>
		
		<option value="UG">UGANDA</option>
		
		<option value="UA">UKRAINE</option>
		
		<option value="AE">UNITED ARAB EMIRATES</option>
		
		<option value="GB">UNITED KINGDOM</option>
		
		<option value="US">UNITED STATES OF AMERICA</option>
		
		<option value="UY">URUGUAY</option>
		
		<option value="UZ">UZBEKISTAN</option>
		
		<option value="VU">VANUATU</option>
		
		<option value="VE">VENEZUELA</option>
		
		<option value="VN">VIETNAM</option>
		
		<option value="VI">VIRGIN ISLANDS, U.S.</option>
		
		<option value="YE">YEMEN ARAB REPUBLIC</option>
		
		<option value="ZM">ZAMBIA</option>
		
	</select>
	</td>
	</tr>
	

	

	

	

	
	<tr>
	<td class="right-align bold" width="30%"> Destination</td>
	<td colspan="2"><font face="vardana" size="1"><input name="destination" class="longtext" type="text">
	</font>
	
	</td>
	</tr>
	

	

	
	<tr>
	<td class="right-align bold" width="30%"> Agent</td>
	<td colspan="2" align="left"><font face="vardana" size="1"><input name="agent" class="longtext" type="text">
	
	</font></td>
	</tr>
	

	

	
	<tr>
	<td class="right-align bold" width="30%"> Country of Citizenship</td>
	<td align="left"><input name="citizenship" class="longtext" type="text">
	
	&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>
	

	


	

	

	

	

	

	

	

	

	<!-- WORK RECORD -->
	
	

	</tbody></table>

<div class="section_description">
	<p>
	
	
			</p>
	</div>


	
	<br>
	
	<div class="basic bold_color header">
		Guardian Contact Information
	</div>
	
<div class="section_description">
	<p>
		
	</p>
	</div>

	<table>


	

	<tbody><tr>
	<td class="right-align bold" width="30%"> Guardian Name</td>
	<td><input name="guardianname" class="longtext" type="text">
	
	
		&nbsp;<span class="required">  (Required) </span>
	
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%"> Relationship</td>
	<td><input name="guardianrelationship" class="longtext" type="text">
	
	
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%"> Phone</td>
	<td><input name="guardianphone" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%"> Email</td>
	<td><input name="guardianemail" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<!-- COLUMN 2 -->
	
	

	<tr>
	<td class="right-align bold" width="30%"> Address</td>
	<td align="left">
	<input name="guardianaddress" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%">
	 City
	</td>
	<td>
	<input name="guardiancity" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>


	<tr>
	<td class="right-align bold" width="30%">
	 State/Province
	</td>
	<td align="left">
	<input name="guardianstate" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>


	<tr>
	<td class="right-align bold" width="30%">
	 Zip/Postcode
	</td>
	<td>
	<input name="guardianzip" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>


	<tr>
	<td class="right-align bold" width="30%">
	 Country
	</td>
	<td align="left">
	<input name="guardiancountry" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	</tbody></table>

	

	<div class="section_description">
	<p>
      
	</p>
	</div>


	

	<br>
	
	
	<div class="basic bold_color header">
		Permanent Contact Information
	</div>
	
	
	<div class="section_description">
	<p>
		
	</p>
	</div>

	<table>

	

	<tbody><tr>
	<td class="right-align bold" width="30%"> Address</td>
	<td>
	<input name="permanentaddress" class="longtext" type="text">
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%">
	 City
	</td>
	<td>
	<input name="permanentcity" class="longtext" type="text">
	</td>
	</tr>

	<tr>
	<td class="right-align bold" width="30%">
	 State/Province
	</td>
	<td>
	<input name="permanentstate" class="longtext" type="text">
	</td>
	</tr>

	<tr>
	<td class="right-align bold" width="30%">
	 Zip/Postcode
	</td>
	<td align="left">
	<input name="permanentzip" class="longtext" type="text">
	</td>
	</tr>

	<tr>
	<td class="right-align bold" width="30%">
	 Country
	</td>
	<td align="left">
	<input name="permanentcountry" class="longtext" type="text">
	</td>
	</tr>

	

	<!-- COLUMN 2 -->

	

	<tr>
	<td class="right-align bold" width="30%"> Phone</td>
	<td align="left"><input name="permanentphone" class="longtext" type="text"></td>
	</tr>

	

	</tbody></table>

	
	

	<div class="section_description">
	<p>
	
	</p>
	</div>

	

	<br>
	
	
	<div class="basic bold_color header">
		Emergency Contact Information
	</div>
	
	
	<div class="section_description">
	<p>
		
	</p>
	</div>

	<table>

	

	<tbody><tr>
	<td class="right-align bold" width="30%"> Contact Name</td>
	<td> <input name="emercontactname" class="longtext" type="text">
	
	
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%"> Relationship</td>
	<td><input name="emercontactrelationship" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	

	</td>
	</tr>

	

	<!-- COLUMN 2 -->
	
	

	<tr>
	<td class="right-align bold" width="30%"> Home Phone</td>
	<td align="left">
	<input name="emercontacthomephone" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%">
	 Work Phone
	</td>
	<td align="left">
	<input name="emercontactworkphone" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	
	</td>
	</tr>

	

	<tr>
	<td class="right-align bold" width="30%">
	 Cell Phone
	</td>
	<td align="left">
	<input name="emercontactcellphone" class="longtext" type="text">
		
		&nbsp;<span class="required">  (Required) </span>
	

	</td>
	</tr>

	


	</tbody></table>

	

	<div class="section_description">
	<p>
		
		</p>
	</div>
	

	<br>
	
	
	<div class="basic bold_color header">
		<a name="#DISABILITY">Disability Information</a>
	</div>
	
		
		<div class="section_description">
		<p>
			
		</p>
		
		</div>

	<table>

	

	<tbody><tr>
	<td class="right-align bold" width="30%"> Disability</td>
	<td align="left" nowrap="nowrap">
		<input name="isdisabilities" value="1" type="radio"> Yes
		<input name="isdisabilities" value="0" type="radio"> No
		<input name="isdisabilities" value="2" checked="checked" type="radio"> Not Selected
	</td>
	</tr>

	<tr>
	<td class="right-align bold" width="30%"> Description</td>
	<td align="left">
		<textarea rows="3" cols="40" name="disabilities"></textarea>
	</td>
	</tr>



	

	<!-- COLUMN 2 -->
	
	

	<tr>
	<td class="right-align bold" width="30%"> Learning Difficulties</td>
	<td align="left" nowrap="nowrap">
		<input name="islearningdifficulties" value="1" type="radio"> Yes
		<input name="islearningdifficulties" value="0" type="radio"> No
		<input name="islearningdifficulties" value="2" checked="checked" type="radio"> Not Selected
	</td>
	</tr>

	<tr>
	<td class="right-align bold" width="30%"> Description</td>
	<td align="left">
		<textarea rows="3" cols="40" name="learningdifficulties"></textarea>
	</td>
	</tr>

	
	

	

	</tbody></table>
	
		<div class="section_description">
	<p>
	

	</p>
	</div>

	



	

	


	<!-- Student Record Sections-->
	
			<br>
			
			
			<div class="basic bold_color header">
				Parent's Information
			</div>
			
			<br>
			<div class="section_description">
			<p>
				

			</p>
			</div>

			<table>
			<tbody><tr><td class="right-align bold" width="30%">Father's Name</td><td align="left"><input name="XP1197" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Mother's Name</td><td align="left"><input name="XP1198" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Address</td><td align="left"><input name="XP1199" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">City</td><td align="left"><input name="XP1200" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Zip/Postal Code</td><td align="left"><input name="XP1201" class="shorttext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Country</td><td align="left"><input name="XP1202" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Home Phone</td><td align="left"><input name="XP1203" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Work Phone</td><td align="left"><input name="XP1204" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">FAX</td><td align="left"><input name="XP1205" class="shorttext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Mobile Phone</td><td align="left"><input name="XP1206" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">E Mail</td><td align="left"><input name="XP1207" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr>
			</tbody></table>

			<div class="section_description">
			<p>
				

			</p>
			</div>
			
			<br>
			
			
			<div class="basic bold_color header">
				Educational Information
			</div>
			
			<br>
			<div class="section_description">
			<p>
				

			</p>
			</div>

			<table>
			<tbody><tr><td class="right-align bold" width="30%">Institution Attended(1)</td><td align="left"><input name="XP1208" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(1)</td><td align="left"><input name="XP1214" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(1)</td><td align="left"><input name="XP1220" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(1)</td><td align="left"><input name="XP1227" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Institution Attended(2)</td><td align="left"><input name="XP1209" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(2)</td><td align="left"><input name="XP1215" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(2)</td><td align="left"><input name="XP1221" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(2)</td><td align="left"><input name="XP1228" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Institution Attended(3)</td><td align="left"><input name="XP1210" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(3)</td><td align="left"><input name="XP1216" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(3)</td><td align="left"><input name="XP1222" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(3)</td><td align="left"><input name="XP1226" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Institution Attended(4)</td><td align="left"><input name="XP1211" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(4)</td><td align="left"><input name="XP1217" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(4)</td><td align="left"><input name="XP1223" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(4)</td><td align="left"><input name="XP1229" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Institution Attended(5)</td><td align="left"><input name="XP1212" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(5)</td><td align="left"><input name="XP1218" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(5)</td><td align="left"><input name="XP1224" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(5)</td><td align="left"><input name="XP1230" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Institution Attended(6)</td><td align="left"><input name="XP1213" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Dates of Attendance(6)</td><td align="left"><input name="XP1219" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Qualification with Group(6)</td><td align="left"><input name="XP1225" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">GPA/Class(6)</td><td align="left"><input name="XP1231" class="shorttext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr>
			</tbody></table>

			<div class="section_description">
			<p>
				

			</p>
			</div>
			
			<br>
			
			
			<div class="basic bold_color header">
				Work Experience
			</div>
			
			<br>
			<div class="section_description">
			<p>
				

			</p>
			</div>

			<table>
			<tbody><tr><td class="right-align bold" width="30%">Employers Name &amp; Address(1)</td><td align="left"><input name="XP1232" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Position &amp; Title(1)</td><td align="left"><input name="XP1237" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Period Employed(1)</td><td align="left"><input name="XP1242" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Employers Name &amp; Address(2)</td><td align="left"><input name="XP1233" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Position &amp; Title(2)</td><td align="left"><input name="XP1238" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Period Employed(2)</td><td align="left"><input name="XP1243" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Employers Name &amp; Address(3)</td><td align="left"><input name="XP1234" class="longtext" type="text">&nbsp; <span class="required">  (Required) </span> <font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Position &amp; Title(3)</td><td align="left"><input name="XP1239" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Period Employed(3)</td><td align="left"><input name="XP1244" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Employers Name &amp; Address(4)</td><td align="left"><input name="XP1235" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Position &amp; Title(4)</td><td align="left"><input name="XP1240" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Period Employed(4)</td><td align="left"><input name="XP1245" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr><tr><td class="right-align bold" width="30%">Employers Name &amp; Address(5)</td><td align="left"><input name="XP1236" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Position &amp; Title(5)</td><td align="left"><input name="XP1241" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i></i></font></td></tr><tr><td class="right-align bold" width="30%">Period Employed(5)</td><td align="left"><input name="XP1246" class="longtext" type="text"><font face="Arial, Arial, Helvetica" size="1"><i>From &amp; To</i></font></td></tr>
			</tbody></table>

			<div class="section_description">
			<p>
				

			</p>
			</div>
			

	
	
	<br>
	<!-- Tuition And Payment -->
	

	<br>
	
	<div class="basic bold_color header">
		Tuition &amp; Fees Payment Information
	</div>
	
	<div class="section_description">
	<p>
		
	</p>
	</div>

	<table>


	

	<tbody><tr style="display:none">
	<td class="right-align bold" width="30%"> Tuition Rate</td>
	<td><select name="tuitionratetype">

	
	</select>
	
	</td>
	</tr>

	

	<tr style="display:none">
	<td class="right-align bold" width="30%"> Payment Method</td>
	<td align="left"><select name="paymentmethod">
	<option value="1" selected="selected">Cash/Check</option>
	<option value="2">Direct Deposit</option>
	<option value="3">Tuition management company</option>
	
	</select>
	</td>
	</tr>

	

	<!-- BILLING NAME -->

	

	<!-- BILLING STREET -->

	

	<!-- BILLING CITY -->

	<!--COLUMN 2 -->
	
	
	
	<tr>
	<td class="right-align bold" width="30%"> Payment Plan</td>
	<td align="left">

	<input name="paymentplanid" value="0" checked="checked" type="radio">Full Payment<br>
	
           <input name="paymentplanid" value="97" type="radio">2 installments<br>
			
           <input name="paymentplanid" value="96" type="radio">4 installments<br>
			

	<font face="Arial, Arial, Helvetica" size="1">
	All tuition and fees will be billed according to this payment plan.
	</font></td>
	</tr>

	

	

	<!-- ZIP COUNTRY PHONE -->

	

	<!-- EMAIL -->


	</tbody></table>

	<div class="section_description">
	<p>
		
		</p>
	</div>
	

	
        <br>
	<table>
	<tbody><tr>
	<td width="30%"></td>
	<td align="left">
	<font face="arial" size="1">
        <input name="agree" type="checkbox">By submitting this form, I 
hereby declare that I have read, understood and accept the conditions of
 the University Catalog &amp; status.
I agree to abide by the Universitys rules and regulations. I certify 
that the information provided in this application form is true. I
understand that providing false information may result in immediate 
dismissal or discontinuation and forfeiture of financial payments
and academic credits in Southern University. By signing also means, I 
authorize Southern University to use my still photograph and
my audio video clip to development purpose.
	</font>
	</td>
	</tr>	
	</tbody></table>
	
	
	<br>
	<br>
	
	<input style="height:40px; font-face:arial; font-size:14pt; font-weight:bold;" value="Save &amp; Continue Later" onclick="#" type="button">
	<input style="height:40px; font-face:arial; font-size:14pt; font-weight:bold;" value="Submit Application" onclick="#" type="button">

	</form>

	</div>


	<script language="javascript">

	function showAlert(str) {
		alert(TextToHTML(str));
	}

	function TextToHTML(str) {
		var newDiv = document.createElement(newDiv);
		newDiv.innerHTML = str;
		return newDiv.innerHTML
	}

	function isDataValid() {
		var datavalid = true;

		if (document.frmaddstudent.firstname.value == ""){
			datavalid = false;
			showAlert("First Name is Required.");
			document.frmaddstudent.firstname.focus();

		}

		if (datavalid && document.frmaddstudent.lastname.value == ""){
			datavalid = false;
			showAlert("Last name is required.");
			document.frmaddstudent.lastname.focus();
		}

		//STUDENT ONLINE REGISTRATION ALERT START
		
				
			if (datavalid && (document.frmaddstudent.homephone.value == "" && document.frmaddstudent.cellphone.value == "")){
				datavalid = false;
				showAlert("Please enter Home Phone or Cell Phone.");
				document.frmaddstudent.homephone.focus();
			}
		

		

		
			if (datavalid && document.frmaddstudent.email.value == ""){
				datavalid = false;
				showAlert("Email is required.");
				document.frmaddstudent.email.focus();
			}
		
		
		
								
		
		
		
		

	
		
		

		
		

		
			if (datavalid && document.frmaddstudent.picturefile.value==""){
				datavalid = false;
				showAlert("Picture File is required.");
				document.frmaddstudent.picturefile.focus();
			}
		


		
			if (datavalid && document.frmaddstudent.dob.value==""){
			datavalid = false;
			showAlert("Birth Date is Required.");
			document.frmaddstudent.dob.focus();
			}
		

		

		

		

		

		
			if (datavalid && document.frmaddstudent.citizenship.value==""){
				datavalid = false;
				showAlert("Nationality is required.");
				document.frmaddstudent.citizenship.focus();
			}
		

		

		

		

		

		

		

		
			if (datavalid && document.frmaddstudent.guardianname.value==""){
				datavalid = false;
				showAlert("Guardian Name is Required.");
				document.frmaddstudent.guardianname.focus();
			}
			if (datavalid && document.frmaddstudent.guardianrelationship.value==""){
				datavalid = false;
				showAlert("Guardian Relationship is Required.");
				document.frmaddstudent.guardianrelationship.focus();
			}
			if (datavalid && document.frmaddstudent.guardianaddress.value==""){
				datavalid = false;
				showAlert("Guardian Street is Required.");
				document.frmaddstudent.guardianaddress.focus();
			}
			if (datavalid && document.frmaddstudent.guardiancity.value==""){
				datavalid = false;
				showAlert("Guardian City is Required.");
				document.frmaddstudent.guardiancity.focus();
			}
			if (datavalid && document.frmaddstudent.guardianstate.value==""){
				datavalid = false;
				showAlert("Guardian State/Province is required.");
				document.frmaddstudent.guardianstate.focus();
			}
			if (datavalid && document.frmaddstudent.guardianzip.value==""){
				datavalid = false;
				showAlert("Guardian Zip/Postcode is Required.");
				document.frmaddstudent.guardianzip.focus();
			}
			if (datavalid && document.frmaddstudent.guardiancountry.value==""){
				datavalid = false;
				showAlert("Guardian Country is Required.");
				document.frmaddstudent.guardiancountry.focus();
			}
			if (datavalid && document.frmaddstudent.guardianphone.value==""){
				datavalid = false;
				showAlert("Guardian Phone is Required.");
				document.frmaddstudent.guardianphone.focus();
			}
			if (datavalid && document.frmaddstudent.guardianemail.value==""){
				datavalid = false;
				showAlert("Guardian Email is Required.");
				document.frmaddstudent.guardianemail.focus();
			}
		

		
			if (datavalid && document.frmaddstudent.permanentaddress.value==""){
				datavalid = false;
				showAlert("Permanent Street is required.");
				document.frmaddstudent.permanentaddress.focus();
			}
			if (datavalid && document.frmaddstudent.permanentcity.value==""){
				datavalid = false;
				showAlert("Permanent City is required.");
				document.frmaddstudent.permanentcity.focus();
			}
			if (datavalid && document.frmaddstudent.permanentstate.value==""){
				datavalid = false;
				showAlert("Permanent State/Province is required.");
				document.frmaddstudent.permanentstate.focus();
			}
			if (datavalid && document.frmaddstudent.permanentzip.value==""){
				datavalid = false;
				showAlert("Permanent Zip/Postcode is required.");
				document.frmaddstudent.permanentzip.focus();
			}
			if (datavalid && document.frmaddstudent.permanentcountry.value==""){
				datavalid = false;
				showAlert("Permanent Country is required.");
				document.frmaddstudent.permanentcountry.focus();
			}
			if (datavalid && document.frmaddstudent.permanentphone.value==""){
				datavalid = false;
				showAlert("Permanent Phone is required.");
				document.frmaddstudent.permanentphone.focus();
			}
		

		
			if (datavalid && document.frmaddstudent.emercontactname.value==""){
				datavalid = false;
				showAlert("Emergency Contact Name is required.");
				document.frmaddstudent.emercontactname.focus();
			}
			if (datavalid && document.frmaddstudent.emercontactrelationship.value==""){
				datavalid = false;
				showAlert("Emergency Contact Relationship is required.");
				document.frmaddstudent.emercontactrelationship.focus();
			}
			if (datavalid && document.frmaddstudent.emercontacthomephone.value==""){
				datavalid = false;
				showAlert("Emergency Contact Home Phone is required.");
				document.frmaddstudent.emercontacthomephone.focus();
			}
			if (datavalid && document.frmaddstudent.emercontactworkphone.value==""){
				datavalid = false;
				showAlert("Emergency Contact Work Phone is required.");
				document.frmaddstudent.emercontactworkphone.focus();
			}
			if (datavalid && document.frmaddstudent.emercontactcellphone.value==""){
				datavalid = false;
				showAlert("Emergency Contact Cell Phone is required.");
				document.frmaddstudent.emercontactcellphone.focus();
			}

		

		
			if (datavalid && document.frmaddstudent.disabilities.value==""){
				datavalid = false;
				showAlert("Disability Description is required.");
				document.frmaddstudent.disabilities.focus();

			}

			if (datavalid && document.frmaddstudent.learningdifficulties.value==""){
				datavalid = false;
				showAlert("Disability Learning Difficulties is required.");
				document.frmaddstudent.learningdifficulties.focus();
			}
		
	

		//STUDENT ONLINE REGISTRATION  ALERT END


		//STUDENT ONLINE REGISTRATION EXTENDED FIELD ALERT START
		
				if (datavalid && document.frmaddstudent.XP1189.value==""){//5
					datavalid = false;
					showAlert("Student Full Name is required.");
					document.frmaddstudent.XP1189.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1255.value==""){//5
					datavalid = false;
					showAlert("Blood Group is Required.");
					document.frmaddstudent.XP1255.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1196.value==""){//5
					datavalid = false;
					showAlert("Place of Birth is required.");
					document.frmaddstudent.XP1196.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1194.value==""){//5
					datavalid = false;
					showAlert("Work Phone is required.");
					document.frmaddstudent.XP1194.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1197.value==""){//5
					datavalid = false;
					showAlert("Father's Name is Required.");
					document.frmaddstudent.XP1197.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1198.value==""){//5
					datavalid = false;
					showAlert("Mother's Name is required.");
					document.frmaddstudent.XP1198.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1199.value==""){//5
					datavalid = false;
					showAlert("Address is Required.");
					document.frmaddstudent.XP1199.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1200.value==""){//5
					datavalid = false;
					showAlert("City is required.");
					document.frmaddstudent.XP1200.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1202.value==""){//5
					datavalid = false;
					showAlert("Country is Required.");
					document.frmaddstudent.XP1202.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1203.value==""){//5
					datavalid = false;
					showAlert("Home Phone is Required.");
					document.frmaddstudent.XP1203.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1204.value==""){//5
					datavalid = false;
					showAlert("Work Phone is required.");
					document.frmaddstudent.XP1204.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1206.value==""){//5
					datavalid = false;
					showAlert("Mobile Phone is required.");
					document.frmaddstudent.XP1206.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1207.value==""){//5
					datavalid = false;
					showAlert("E Mail is required.");
					document.frmaddstudent.XP1207.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1208.value==""){//5
					datavalid = false;
					showAlert("Institution Attended(1) is required.");
					document.frmaddstudent.XP1208.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1214.value==""){//5
					datavalid = false;
					showAlert("Dates of Attendance(1) is Required.");
					document.frmaddstudent.XP1214.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1220.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(1) is required.");
					document.frmaddstudent.XP1220.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1227.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(1) is Required.");
					document.frmaddstudent.XP1227.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1209.value==""){//5
					datavalid = false;
					showAlert("Institution Attended(2) is required.");
					document.frmaddstudent.XP1209.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1215.value==""){//5
					datavalid = false;
					showAlert("Dates of Attendance(2) is Required.");
					document.frmaddstudent.XP1215.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1221.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(2) is required.");
					document.frmaddstudent.XP1221.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1228.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(2) is Required.");
					document.frmaddstudent.XP1228.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1210.value==""){//5
					datavalid = false;
					showAlert("Institution Attended(3) is required.");
					document.frmaddstudent.XP1210.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1216.value==""){//5
					datavalid = false;
					showAlert("Dates of Attendance(3) is Required.");
					document.frmaddstudent.XP1216.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1222.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(3) is required.");
					document.frmaddstudent.XP1222.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1226.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(3) is Required.");
					document.frmaddstudent.XP1226.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1211.value==""){//5
					datavalid = false;
					showAlert("Institution Attended(4) is required.");
					document.frmaddstudent.XP1211.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1223.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(4) is required.");
					document.frmaddstudent.XP1223.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1229.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(4) is Required.");
					document.frmaddstudent.XP1229.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1212.value==""){//5
					datavalid = false;
					showAlert("Institution Attended(5) is required.");
					document.frmaddstudent.XP1212.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1218.value==""){//5
					datavalid = false;
					showAlert("Dates of Attendance(5) is Required.");
					document.frmaddstudent.XP1218.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1224.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(5) is required.");
					document.frmaddstudent.XP1224.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1230.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(5) is Required.");
					document.frmaddstudent.XP1230.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1219.value==""){//5
					datavalid = false;
					showAlert("Dates of Attendance(6) is Required.");
					document.frmaddstudent.XP1219.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1225.value==""){//5
					datavalid = false;
					showAlert("Qualification with Group(6) is required.");
					document.frmaddstudent.XP1225.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1231.value==""){//5
					datavalid = false;
					showAlert("GPA/Class(6) is Required.");
					document.frmaddstudent.XP1231.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1232.value==""){//5
					datavalid = false;
					showAlert("Employers Name & Address(1) is required.");
					document.frmaddstudent.XP1232.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1237.value==""){//5
					datavalid = false;
					showAlert("Position & Title(1) is required.");
					document.frmaddstudent.XP1237.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1242.value==""){//5
					datavalid = false;
					showAlert("Period Employed(1) is required.");
					document.frmaddstudent.XP1242.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1233.value==""){//5
					datavalid = false;
					showAlert("Employers Name & Address(2) is required.");
					document.frmaddstudent.XP1233.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1238.value==""){//5
					datavalid = false;
					showAlert("Position & Title(2) is required.");
					document.frmaddstudent.XP1238.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1243.value==""){//5
					datavalid = false;
					showAlert("Period Employed(2) is required.");
					document.frmaddstudent.XP1243.focus();
				}//5
				
				if (datavalid && document.frmaddstudent.XP1234.value==""){//5
					datavalid = false;
					showAlert("Employers Name & Address(3) is required.");
					document.frmaddstudent.XP1234.focus();
				}//5
				
		//STUDENT ONLINE REGISTRATION EXTENDED FIELD ALERT END


		
			if (datavalid && !document.frmaddstudent.agree.checked){
				datavalid = false;
				showAlert("You must accept the declaration at the bottom of the form.");
			}
		

		return datavalid;
	}
	
	function SubmitPayment()
	{
		document.frmaddstudent.continuelater.value = "no";
		document.frmaddstudent.action = "public_save_student_registration.jsp";
		if (isDataValid()) {		
			document.frmaddstudent.submit();
		}
	}
	
	function SavePayment()
	{
		
		document.frmaddstudent.continuelater.value ="yes";
		if(document.frmaddstudent.email.value == "") {
			showAlert("Email is required.");
			document.frmaddstudent.email.focus();
		}else {
			//document.frmaddstudent.action = "public_save_continue_student_registration.jsp";
			document.frmaddstudent.action = "public_save_student_registration.jsp";
			if (isDataValid()) {		
				document.frmaddstudent.submit();
			}
		}
	}


        function UploadAttachment(attachtype)
	{
		s = window.open("public_browse_file.jsp?attachtype="+ attachtype+ "&adminId=30&description=STUDENT_FILE","attachmentuploadwin","width=540,height=150,top=110,screenY=110,left=157,screenX=157");
		s.focus();
	}

	function DeleteAttachment(attachtype) {
		if (confirm("Are you sure you wish to delete this attachment file?")) {
			document.getElementById("XP" + attachtype).value = "0";
			document.getElementById(attachtype + "link").innerHTML = "";
			document.getElementById(attachtype + "link").href = "";
			document.getElementById("remove" + attachtype + "btn").disabled = true;
		}
	}

	function SetAttachmentProps(attachtype, attachmentfileid, attachmentfilename)
	{
		showAlert("File Successfully Uploaded. Please don't forget to save this student record to access the uploaded file.");

			document.getElementById("XP" + attachtype).value = attachmentfileid + ";" + attachmentfilename.replace(';','^');
			document.getElementById(attachtype + "link").innerHTML = attachmentfilename.replace(';','^');
			document.getElementById(attachtype + "link").href = "getfile.jsp?fileid=" + attachmentfileid;
			document.getElementById("remove" + attachtype + "btn").disabled = false;
	}

	</script>

	
	
	
	<p>
	<br>
	
	
	
	

	

	






</p></center></body></html>