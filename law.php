
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Department of Law</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>     <!--Menu Area -->
    <div id="nav">
           <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="career.php">Academics</a>
          <ul>
            <li><a href="faculties.php" target="_blank">Faculty Members</a></li>
            
            <li><a href="departments.php">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse/index.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php">Law</a></li>             
                 <li><a href="english.php">English</a></li>
           <li><a href="fse/index.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharmacy/index.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php">Civil Engineering</a></li>
               <li><a href="islamic.php">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="career.php">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="#">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="#">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                       <li><a href="#"><font color="#FF0000"><strong>Log in</strong></font></a></li>


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of Law</h2>
            <ul>
              <li>
                <div class="desc1">
<h3>FACULTY OF ARTS SOCIAL SCIENCE & LAW</h3></br></br>
Department of Law</br>
<b>Bachelor of Law (LLB)</b></br>
General Education, Core & Elective Courses: 108 semester credit hours</br></br>

<p align="justify">The Four-year curriculum develops students' analytical ability, knowledge of the theory and practice of law, communications skills, and an understanding of the codes of professional responsibility and ethics central to the practice of law. Students experience a variety of teaching methods, including the traditional case and Socratic methods, as well as problems, simulations, role-playing, computer-assisted instruction and interaction with actual clients. After completing first-year requirements, students can tailor their course load to fit their interests and career plans.</p><br /><br />
 
<center><b>Bachelor in Law (LLB 4 Years)</b></center><br /><br />
<p align="justify">Bachelor in Law (LLB) is four years degree providing quality legal education at the undergraduate levels. Course work features practices, as well enable students to work effectively in business environment. The Bachelor in Law (LLB) degree is 4 years program, requires 126 semester hours of post secondary college-level study. This required semester credit hours depending upon the academic background in English and Math of the student.</p><br /><br />

EN 1010 English Composition<br />
EN 1210 Spoken English<br />
EN 1220 English Expository Writing<br />
IR 1010 International Relations<br />
SO 1010 Contemporary To Sociology<br />
BS 1010 Bangladesh Studies<br />
CS 1000 Computer Fundamentals & Application<br />
ES 1010 Environmental Science<br />
BI 1010 Health Science Management<br />
LL 2001 The Legal System of Bangladesh<br />
LL 2005 Jurisprudence<br />
LL 2010 Legal History of The South Asian Sub-Continent<br />
LL 2015 Muslim Law<br />
LL 2020 The Law of Tort<br />
LL 2022 Law of Contract<br />
LL 2025 Equity Trust & Hindu Law<br />
LL 2035 Land Laws of Bangladesh<br />
LL 2030 Transfer of Property Act<br />
LL 2035 Registration Act and Public Demand Recovery Act<br />
LL 2040 Constitutional Law Of Bangladesh<br />
LL 2045 Law of Evidence<br />
LL 2050 Law of Taxation<br />
LL 2055 Law of Crimes (Substantive) [1]<br />
LL 2056 Law of Crimes (Substantive) [2]<br />
LL 2057 Law of Crimes (Procedural) [1]<br />
LL 2058 Law of Crimes (Procedural) [2]<br />
LL 2060 Code of Criminal Procedure<br />
LL 2065 International Law<br />
LL 2069 Law of Limitation and Specific Relief Act<br />
LL 2075 Mercantile Law & Other Business Laws<br />
LL 2085 Law of Conveyancing, Pleadings & Legal Drafting<br />
LL 2086 Professional Ethics & Morality<br />
LL 3010 Administrative Law<br />
LL 3110 Company Law<br />
LL 3140 Constitutional Law of U.K & U.S.A<br />
LL 3180 The Government and Politics<br />
LL 3210 Labor and Industrial Law <br />
LL 4010 Environmental Laws<br />
LL 4015 Moot Court, Mock Trail & Court Attendance<br /><br />

<center><b>Bachelor in Law (LLB 2 Years)</b></center><br /><br />
<p align="justify">The Two-year curriculum develops students' knowledge of the theory and practice of law, communications skills, and an understanding of the codes of professional responsibility and ethics central to the practice of law. Students experience a variety of teaching methods, including the traditional case and Socratic methods, as well as problems, simulations, role-playing, and interaction with actual clients. 2 years LLB program is 60-credit 20 courses degree providing quality law education at professionals levels. Course work features practices, as well enable students to work effectively.</p><br /><br />

<b>General Education:</b> 9 Courses X 3 Credit = 27 Credit Hours<br />
<b>Core:</b> 26 Courses X 3 Credit = 78 Credit Hours<br />
<b>Project:</b> 1 Project =6 Credit Hours =6 Credit Hours<br /><br />

<b>Optional (For Selective Students)</b><br /><br />
GD 1010 Career Planing & Development<br />
EN 1002 English Reading<br />
Military Science<br /><br />

<center><b>Master of Law (LLM) Program</b><br />
Core & Elective Courses: 36-60 semester credit hours</center><br /><br />

<p align="justify">The courses of study for the Degree of Master of Law (LL.M) shall extend over for a period of one (1) year 30 credits (8 x3=24+6=30) for the students who have achieved 4 years LL.B (Honours).</p><br /><br />
It will be for a period of two (2) years covering 60 credits (16 x3=48+6=48) for the students who have completed 2 years LL.B Degree.<br /><br />

1. Principles of Civil Litigation & Court Management.<br />
2. Law of Human Rights.<br />
3. Intellectual Property Law & Development.<br />
4. Law of International Organizations.<br />
5. Globalization & Comparative Legal System.<br />
6. Admiralty And Maritime Law.<br />
7. Alternative Dispute Resolution.<br />
8. International Refugee Law & Migration.<br />
9. Administrative Law.<br />
10. International Criminal Justice.<br />
11. Environmental Law.<br />
12. European Union Law.<br />
13. Corporate Law.<br />
14. Muslim Family Law.<br />
15. Project & Thesis.<br /><br />

<b>Optional</b><br /><br />

1. Research Methodology.<br />
2. Legal And Ethical Aspects of Business.<br />
3. International Humanitarian Law.<br />
4. Gender Justice.

                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
    <div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="#">Academic Calendar</a></li>
              <li><a href="#">Admissions 2013</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#" target="_blank">Student Portal</a></li>
              <li><a href="#" target="_blank">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i>" Committed to Academic Excellence  "</i></p>
            <em class="shoutright">&nbsp;</em> </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="http://www.southern-bd.info" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call: +880 1824455500</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>&copy; 2013 Southern University Bangladesh - All Rights Reserved -</br><b>Developed by SUB IT-Department</b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</div></body>
</html>
