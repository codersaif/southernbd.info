
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Financial Assistance</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>    <!--Menu Area -->
    <div id="nav">
            <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Financial Assitance for Students</h2>
            <ul>
              <li>
                <div class="desc1">
<p align="justify">Education is an investment. We support the students in making the worthy investment through our financial aid office. Assistance is available to all students who qualify at Southern University. These aids come in various forms, including local and international private scholarships, work-study programs and part-time jobs. A limited number of scholarships are available to those students who demonstrated exceptional academic achievement at their previous academic institute. You are encouraged to visit the finance/accounts office and meet with the staff to determine if you qualify for assistance. The University financial aid professionals are dedicated to develop an aid package that gives you the best chance of earning an education without overwhelming financial worries. Application for all financial aid and scholarships is included in the application for admission. The statement of financial support must accompany the application for admission.</p><br />

<b>Scholarship for academic achievement</b><br /><br />

<p align="justify">Southern University scholarships are based solely on student's previous academic records. The required information is supplied as part of the application for admission. The admission office uses official transcripts and official test scores to identify eligible first-time freshmen students. Students are required to have a 3.5 college grade point average. Scholarships are renewable each year provided the recipient maintains a 3.5 grade point average each academic year. It is possible however to reduce the cost at University program by qualifying for academic and student scholarships. Financial aid and scholarships not cover the costing books, lab fees, exam fees, and graduation fees. Please consider the cost of Southern University before you continue with your application process. This is the student's responsibility. </p><br />               

<b>Requirements for student's aid/scholarships</b><br /><br />

Good academic standing is determined by measuring the student's academic performance at University and consists of the following:<br /><br />

<p align="justify">1) <b>Satisfactory academic progress-</b> In order to meet the satisfactory academic progress requirement, the student must maintain a cumulative grade point average greater than that which would result in academic dismissal.</p><br /> 
<p align="justify">2) <b>Program requirement-</b>In order to meet the pursuit of program requirement, the student must pass a minimum number of credit hours each semester and complete all degree requirements within a specified number of semesters.</p><br /> 
<p align="justify">3) <b>Consideration-</b> Following each semester, the cumulative GPA and number of credits earned by each student are reviewed for complainer with the criteria for good academic standing. Students not receiving financial aid are subject to the same criteria and can be placed on financial aid probation or suspension for future consideration.</p><br /> 
<p align="justify">4) <b>Probation-</b> A student who fails to meet the above criteria in any semester is placed on financial aid probation. A student placed on financial aid probation may receive financial aid for the next three (3) semesters; student failing to maintain good academic standing can be placed on financial aid probation at Southern University, Bangladesh.</p><br /> 
<p align="justify">5) <b>Suspension-</b> A student is placed on financial aid suspension if the student fails to pass any credit hours or withdrawing from classes. If a student on financial aid probation does not regain good academic standing by the end of the probationary period. The student is placed on financial aid suspension. Any student who regains good academic standing and then loses it during a subsequent semester is also placed on financial aid suspension. Violation of any rule of Southern University a disqualification for enjoying scholarship/any financial aid.</p><br /> 

<b>Important Information</b> Scholarships are subject to change periodically. Each year new scholarships will be offered and some will be discontinued.<br /> <br /> 

<b>Student Employment</b><br /> <br />

<p align="justify">A part time job can be a valuable and rewarding experience. Some part-time jobs require students to demonstrate their financial need (work-study), although a job cannot be guaranteed.</p>
                
                </div>
              </li>
             </ul>
          </div>
  
        </div>
           <br class="clear" />
        </div>
      </div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>