
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Department of Islamic Studies</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>     <!--Menu Area -->
    <div id="nav">
            <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="career.php">Academics</a>
          <ul>
            <li><a href="faculties.php" target="_blank">Faculty Members</a></li>
            
            <li><a href="departments.php">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse/index.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php">Law</a></li>             
                 <li><a href="english.php">English</a></li>
           <li><a href="fse/index.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharmacy/index.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php">Civil Engineering</a></li>
               <li><a href="islamic.php">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="career.php">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="#">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="#">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                       <li><a href="#"><font color="#FF0000"><strong>Log in</strong></font></a></li>


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of Islamic Studies</h2>
            <ul>
              <li>
                <div class="desc1">
<h3>FACULTY OF ARTS SOCIAL SCIENCE & LAW</h3></br></br>
Department of Islamic Studies</br>
<b>Master of Arts in Islamic Studies (MA in IS)</b></br>
General Education, Core & Elective Courses: 30-48 semester credit hours</br>
All Courses are three (3) Credit hour, unless it mention</br></br>

<p align="justify"><b>Curriculum Two Years MA in IS Courses:</b> MA IS Programs are designed for graduate studies in the Islamic field and modern society. A maximum of 48(16x 3 courses) semester credit hours are required for the Master's degree (MA IS) for 2 years duration. It will continue for a period of two (2) years for the students who have completed Bachelor degree from other disciplines depending on the student academic background.</p><br /><br />

<b>Requirements: 16 Courses × 3 Credit Hours= 48 Credit Hours</b></br></br>

1. IS-5001 Ulum al Qur'an</br>
2. IS -5002 Ulum al Hadith</br>
3. IS-5003 Sources of Islam: Al Qur'an</br>
4. IS-5004 Sources of Islam: Al Hadith</br>
5. IS 5005 Biography of Muhammad(S.A.W)</br>
6. IS 5006 Islam and Science</br>
7. IS-5007 Islamic 'Aqidah- Al Kalam</br>
8. IS-5008 Islamic Philosophy</br>
9. IS-6001 Comparative Religion</br>
10. IS-6002 Miraculous Nature of Qur'an</br>
11. IS-6003 Sufism</br>
12. IS-6004 Islamic Ethics</br>
13. IS-6005 Typology of Knowledge in Islam</br>
14. IS-6006 Schools of Shari'ah</br>
15. IS-6007 Islamic Economic System</br>
16. IS-6008 Islamic Political Thought</br></br>

<b>Optional:</b></br></br>

1. EN 1010 Spoken English</br>
2. IS 1013 Spoken Arabic</br>
3. CS-1000, Computer Fundamentals & Application</br>
4. IS-6011 Research Methodology</br>
5. IS-6012 Thesis</br></br>

<p align="justify"><b>One Year Masters Courses:</b> The courses of study for the Degree of Master of IS (MA IS) shall extend over a period of one (1) year for the students who have acquired 4 years BA IS (Honors). A minimum of 24 (8 x 3 courses) semester credit hours with a project (6 cr/hrs)is required to the Master degree( MA IS) for 4 years bachelor degree holders in Islamic Studies, depending on student's academic background .</p><br /><br />

<b>Requirements: 10 Courses × 3 Credit Hours= 30 Credit Hours</b></br></br>

1. IS-5001 Ulum al Qur'an</br>
2. IS -5002 Ulum al Hadith</br>
3. IS-5005, Biography of Muhammad (S.A.W.)</br>
4. IS-5007 Islamic 'Aqidah- Al Kalam</br>
5. IS-5008 Islamic Philosophy</br>
6. IS-6001 Comparative Religion</br>
7. IS-6002 Miraculous Nature of Qur'an</br>
8. IS-6006 Schools of Shari'ah</br>
9. IS-6007 Islamic Economic System</br>
10. IS-6008 Islamic Political Thought</br></br>

<b>Optional:</b></br></br>

1. IS-6004 Islamic Ethics</br>
2. IS-5003 Sources of Islam Al Qur'an</br>
3. IS-5004 Sources of Islam Al Hadith</br>
4. EN 1010 Spoken English</br>
5. IS 1013 Spoken Arabic</br>
6. CS-1000 Computer Fundamentals & Application</br>
7. IS-6011 Research Methodology</br>
8. IS-6012 Thesis

                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
    <div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="#" target="_blank">Academic Calendar</a></li>
              <li><a href=""#>Admissions 2013</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#" target="_blank">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i>" Committed to Academic Excellence  "</i></p>
            <em class="shoutright">&nbsp;</em> </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:+880 1824455500</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>&copy; 2013 Southern University Bangladesh - All Rights Reserved -</br><b>Developed by SUB IT-Department</b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</div></body>
</html>