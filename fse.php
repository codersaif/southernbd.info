
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Member of Trustee Board</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	color: #000066;
}
.style2 {
	font-size: 18px;
	font-weight: bold;
	font-style: italic;
}
-->
</style>
</head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>  <!--Menu Area -->
    <div id="nav">
       <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="career.php">Academics</a>
          <ul>
            <li><a href="faculties.php" target="_blank">Faculty Members</a></li>
            
            <li><a href="departments.php">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse/index.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php">Law</a></li>             
                 <li><a href="english.php">English</a></li>
           <li><a href="fse/index.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharmacy/index.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php">Civil Engineering</a></li>
               <li><a href="islamic.php">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="career.php">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="#">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="#">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                       <li><a href="#"><strong></strong></font></a></li>


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of CSIT, ECE &amp; EEE</h2>
           
            <ul><li>
              <div class="desc1">
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
	
    <td>
         <p align="center" class="style2"> Southern University Bangladesh</p><br><br>
      <ul>
      </ul>      
      <p class="txt">Southern University began its journey  as an Institute in the year of   1998. Later, in the year 2003, it appears as a  university with the   Government approval under the private Universities act.  Being one of   the leading universities in Chittagong, it has been serving to the    community for a better human resource to meet the demand of a fast   growing city  Chittagong as well as Bangladesh. Since it started serving   the community,  Southern University Bangladesh has been committed to   academic excellence and  quality educations.</p>
      <p class="txt"><br />
        As a part of rapid progression,  the faculty of   science and engineering has been established in 2005. At  present, the   faculty is consists of five departments including Department of Civil    Engineering, Department of Computer Science and Information Technology,    Department of Electronic and Communication Engineering, Department of    Electrical and Electronic Engineering, and Department of Pharmacy.   Southern  University is the first university in Chittagong to introduce   Master of Science  in Computer Science and Information Technology.</p>
      <p class="txt">&nbsp;</p>
      <p class="txt"> The   faculty is fully equipped  with quality faculty members, highly standard   labs, and modern quality teaching  methods. The faculty aims to produce   quality graduates and researchers for the  country so that we can keep   up steps with the develop countries and make our  country an attractive   place to live and work.</p>
      <p class="txt"><div class="static">
               	  <h3 class="colr wlcm"><strong>Bachelor in </strong><strong>Computer Science &amp; Information Technology (CSIT)</strong></h3><br />
               	  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="222" valign="top"><p><strong>Types of Courses</strong></p></td>
                      <td width="212" valign="top"><p align="center"><strong>No. Of Courses</strong></p></td>
                      <td width="185" valign="top"><p align="center"><strong>Credit Hours</strong></p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>English Courses</p></td>
                      <td width="212" valign="top"><p align="center">3</p></td>
                      <td width="185" valign="top"><p align="center">9</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>GED courses</p></td>
                      <td width="212" valign="top"><p align="center">5</p></td>
                      <td width="185" valign="top"><p align="center">15</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Basic Science Courses</p></td>
                      <td width="212" valign="top"><p align="center">3</p></td>
                      <td width="185" valign="top"><p align="center">9</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Mathematics Courses</p></td>
                      <td width="212" valign="top"><p align="center">5</p></td>
                      <td width="185" valign="top"><p align="center">15</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Inter disciplinary Engineering    Courses</p></td>
                      <td width="212" valign="top"><p align="center">6</p></td>
                      <td width="185" valign="top"><p align="center">18</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Program core courses</p></td>
                      <td width="212" valign="top"><p align="center">22</p></td>
                      <td width="185" valign="top"><p align="center">67</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Option I (Specialized course)</p></td>
                      <td width="212" valign="top"><p align="center">1</p></td>
                      <td width="185" valign="top"><p align="center">3</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p>Option II (Specialized courses)</p></td>
                      <td width="212" valign="top"><p align="center">2</p></td>
                      <td width="185" valign="top"><p align="center">6</p></td>
                    </tr>
                    <tr>
                      <td width="222" valign="top"><p align="center"><strong>Total</strong></p></td>
                      <td width="212" valign="top"><p align="center"><strong>47</strong></p></td>
                      <td width="185" valign="top"><p align="center"><strong>142</strong></p></td>
                    </tr>
                  </table>
               	  <p align="center"><strong><u>List of Courses</u></strong></p>
               	  <table border="1" cellspacing="0" cellpadding="0" width="619">
                    <tr>
                      <td width="618" colspan="6" valign="top"><p><strong>Interdisciplinary Courses </strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center"><strong>Course Code</strong><br />
                              <strong>(Old)<u></u></strong></p></td>
                      <td width="289" valign="top"><p align="center"><strong>Course Title<u></u></strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours</strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>Contact Hours / Week<u></u></strong></p></td>
                      <td width="96" colspan="2" valign="top"><p align="center"><strong>Prerequisite Courses<u></u></strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="618" colspan="6" valign="top"><p align="center"><strong>English Courses (Three courses-9 credit    hours)</strong><strong> </strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ENG-1011</p></td>
                      <td width="289" valign="top"><p>English Reading</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ENG-1113</p></td>
                      <td width="289" valign="top"><p>English Composition </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">ENG-1010</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ENG-1215</p></td>
                      <td width="289" valign="top"><p>Spoken English </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="379" colspan="2" valign="top"><p align="center"><strong>Total</strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>9</strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>9</strong></p></td>
                      <td width="96" colspan="2" valign="top"><p align="center"><strong>&nbsp;</strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="618" colspan="6" valign="top"><p align="center"><strong>List of General    Education Courses (any five courses-15 credit hours)</strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ART-1011</p></td>
                      <td width="289" valign="top"><p>Bangladesh Studies</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ART-1113</p></td>
                      <td width="289" valign="top"><p>Introduction to Sociology</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ART- 2113</p></td>
                      <td width="289" valign="top"><p>Professional Ethics</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ECON-2211</p></td>
                      <td width="289" valign="top"><p>Principals of Economics<u> </u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MGT-1215</p></td>
                      <td width="289" valign="top"><p>Introduction to    Business</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ACC-2011</p></td>
                      <td width="289" valign="top"><p>Principles of    Accounting</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MGT-3111</p></td>
                      <td width="289" valign="top"><p>Industrial    Management</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="618" colspan="6" valign="top"><p><strong>Basic Science Courses (Three    courses-9 credit hours)</strong> </p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">PHY-1211</p></td>
                      <td width="289" valign="top"><p>General Physics    I</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">PHY-1212</p></td>
                      <td width="289" valign="top"><p>General Physics:    Sessional </p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">PHY-2113</p></td>
                      <td width="289" valign="top"><p>General Physics    II</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">PHY-2211</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">PHY-2114</p></td>
                      <td width="289" valign="top"><p>General Physics:    Sessional </p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">PHY-2212</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CHEM-2211</p></td>
                      <td width="289" valign="top"><p>Chemistry</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CHEM-2212</p></td>
                      <td width="289" valign="top"><p>Chemistry:    Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="379" colspan="2" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>09</strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="618" colspan="6" valign="top"><p><strong>Mathematics    Courses (5 courses-15 credit hours)</strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MATH-1011</p></td>
                      <td width="289" valign="top"><p>Differential and Integral Calculus </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MATH-1113</p></td>
                      <td width="289" valign="top"><p>Linear Algebra    &amp; Matrix</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">MATH-1011</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MATH-1215</p></td>
                      <td width="289" valign="top"><p>Ordinary    and Partial Differential Equations</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">MATH-1113</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MATH-2011</p></td>
                      <td width="289" valign="top"><p>Co-0rdinate    Geometry and Vector Analysis </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">MATH-2111</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">MATH-2113</p></td>
                      <td width="289" valign="top"><p>Probability and Statistics</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="379" colspan="2" valign="top"><p align="center"><strong>Total</strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>15</strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>15</strong></p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="618" colspan="6" valign="top"><p><strong>Other Engineering Discipline    Courses (Six courses-18 credit hours)</strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center"><strong>Course Code<u></u></strong></p></td>
                      <td width="289" valign="top"><p align="center"><strong>Course Title<u></u></strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours</strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>Contact Hours / Week<u></u></strong></p></td>
                      <td width="96" colspan="2" valign="top"><p align="center"><strong>Prerequisite Courses<u></u></strong></p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CE-2010</p></td>
                      <td width="289" valign="top"><p>Engineering    Drawing: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">3.0</p></td>
                      <td width="71" valign="top"><p align="center">3.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ME-1011</p></td>
                      <td width="289" valign="top"><p>Mechanical Engineering    Fundamentals</p></td>
                      <td width="72" valign="top"><p align="center">2.0</p></td>
                      <td width="71" valign="top"><p align="center">2.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ME-1012</p></td>
                      <td width="289" valign="top"><p>Mechanical Engineering    Fundamentals Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1.0</p></td>
                      <td width="71" valign="top"><p align="center">2.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                      <td width="1"><p>&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90"><p align="right">ECE-1215</p></td>
                      <td width="289"><p>Electronics I</p></td>
                      <td width="72"><p align="center">2.0</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">2.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">ECE-1011 &amp; ECE-1013</p></td>
                    </tr>
                    <tr>
                      <td width="90"><p align="right">ECE-1216</p></td>
                      <td width="289"><p>Electronics    Circuit Simulation Laboratory</p></td>
                      <td width="72"><p align="center">1.0</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">3.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">ECE-1114 &amp; ECE-1116</p></td>
                    </tr>
                    <tr>
                      <td width="90"><p align="right">ECE-2111</p></td>
                      <td width="289"><p>Semiconductor devices </p></td>
                      <td width="72"><p align="center">2.0</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">2.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ECE-3121</p></td>
                      <td width="289" valign="top"><p>Microprocessor, Assembly Language    &amp; Computer Interfacing</p></td>
                      <td width="72" valign="top"><p align="center">3.0</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">3.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">ECE-2113</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">ECE-3122</p></td>
                      <td width="289" valign="top"><p>Microprocessor, Assembly Language    &amp; Computer Interfacing: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1.0</p></td>
                      <td width="72" colspan="2" valign="top"><p>2.0</p></td>
                      <td width="96" colspan="2" valign="top"><p align="center">ECE-2114</p></td>
                    </tr>
                    <tr>
                      <td width="90"><p align="right">ECE-3227 </p></td>
                      <td width="289"><p>Control System I </p></td>
                      <td width="72"><p align="center">2.0</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">2.0</p></td>
                      <td width="96" colspan="2"><p align="center">ECE-3013 </p></td>
                    </tr>
                    <tr>
                      <td width="90"><p align="right">ECE-3228 </p></td>
                      <td width="289"><p>Control    System&nbsp;I&nbsp;Laboratory </p></td>
                      <td width="72"><p align="center">1</p></td>
                      <td width="72" colspan="2" valign="top"><p align="center">2.0</p></td>

                      <td width="96" colspan="2"><p align="center">ECE-3014 </p></td>
                    </tr>
                    <tr>
                      <td width="379" colspan="2"><p align="center"><strong>Total</strong></p></td>
                      <td width="72"><p align="center"><strong>18</strong></p></td>
                      <td width="72" colspan="2" valign="top"><p align="center"><strong>23</strong></p></td>
                      <td width="96" colspan="2"><p align="center"><strong>&nbsp;</strong></p></td>
                    </tr>
                  </table>
               	  <p align="center">&nbsp;</p>
               	  <p align="left">List  of Core Courses  </p>
               	  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="90" valign="top"><p align="center"><strong>Course Code<u></u></strong></p></td>
                      <td width="289" valign="top"><p align="center"><strong>Course Title<u></u></strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong></p></td>
                      <td width="71" valign="top"><p align="center"><strong>Contact Hours / Week<u></u></strong></p></td>
                      <td width="96" valign="top"><p align="center"><strong>Prerequisite Courses<u></u></strong></p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-1011</p></td>
                      <td width="289" valign="top"><p>Introduction to    Computers</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-1012<u></u></p></td>
                      <td width="289" valign="top"><p>Introduction to Computers: Sessional<u> </u></p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A </p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-1113</p></td>
                      <td width="289" valign="top"><p>Computer Ethics and Law</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-1115</p></td>
                      <td width="289" valign="top"><p>Discrete    Mathematics</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2011</p></td>
                      <td width="289" valign="top"><p>Computer    Programming</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1011</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2012</p></td>
                      <td width="289" valign="top"><p>Computer    Programming: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1012</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2113</p></td>
                      <td width="289" valign="top"><p>Numerical Methods</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2114</p></td>
                      <td width="289" valign="top"><p>Numerical Methods: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1122</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2211</p></td>
                      <td width="289" valign="top"><p>Operating System</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1011</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2212</p></td>
                      <td width="289" valign="top"><p>Operating System:    Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1012</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2213</p></td>
                      <td width="289" valign="top"><p>Data Structures    &amp; Algorithms</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-2214</p></td>
                      <td width="289" valign="top"><p>Data Structures &amp; Algorithms:    Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1122</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3011</p></td>
                      <td width="289" valign="top"><p>Object Oriented    Programming</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3012</p></td>
                      <td width="289" valign="top"><p>Object Oriented    Programming: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1122</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3013</p></td>
                      <td width="289" valign="top"><p>Data Warehousing &amp; Data    Mining</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3015</p></td>
                      <td width="289" valign="top"><p>Open Source Tools &amp;    Techniques</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-2241</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3016</p></td>
                      <td width="289" valign="top"><p>Open Source Tools &amp;    Techniques: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-2242</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3017</p></td>
                      <td width="289" valign="top"><p>Database    Management System <u></u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1231</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3018</p></td>
                      <td width="289" valign="top"><p>Database Management System:    Sessional <u></u></p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1232</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3113</p></td>
                      <td width="289" valign="top"><p>Computer    Networking</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-2132 &amp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3114</p></td>
                      <td width="289" valign="top"><p>Computer    Networking: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-2133 &amp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3115</p></td>
                      <td width="289" valign="top"><p>Computer    Architecture &amp; Design</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3117</p></td>
                      <td width="289" valign="top"><p>Distributed    Systems</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3141</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3118</p></td>
                      <td width="289" valign="top"><p>Distributed    Systems: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3142</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3213</p></td>
                      <td width="289" valign="top"><p>Software    Engineering</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-2131</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3215</p></td>
                      <td width="289" valign="top"><p>Internet, Intranet    &amp; Extranet Applications</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3141 </p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3216</p></td>
                      <td width="289" valign="top"><p>Internet, Intranet    &amp; Extranet Applications Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3142 </p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-3217</p></td>
                      <td width="289" valign="top"><p>Theory of    Computation</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4011</p></td>
                      <td width="289" valign="top"><p>Object Oriented    Software Development Using UML</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3131</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4012</p></td>
                      <td width="289" valign="top"><p>Object Oriented    Software Development Using UML: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3131</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4015</p></td>
                      <td width="289" valign="top"><p>Management    Information System</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="71" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4017</p></td>
                      <td width="289" valign="top"><p>System Analysis,    Design and Development</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4018</p></td>
                      <td width="289" valign="top"><p>System Analysis,    Design and Development: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4000</p></td>
                      <td width="289" valign="top"><p>BS Capstone Course    Project</p></td>
                      <td width="72" valign="top"><p align="center">6</p></td>
                      <td width="71" valign="top"><p align="center">12</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4115</p></td>
                      <td width="289" valign="top"><p>Compiler Design    &amp; Construction</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4116</p></td>
                      <td width="289" valign="top"><p>Compiler Design    &amp; Construction: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="71" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="379" colspan="2" valign="top"><p align="center"><strong>Total</strong></p></td>
                      <td width="72" valign="top"><p align="center">67</p></td>
                      <td width="71" valign="top"><p align="center">87</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
               	  <p class="colr wlcm"><strong><u>List of Specialized Elective Courses</u></strong><br />
                  <strong>Option-I for Eleventh Semester </strong>(Any one course and Sessional, if any, to be  taken)</p>
               	  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="90" valign="top"><p align="center"><strong>Course Code</strong><br />
                              <strong>(Old)<u></u></strong></p></td>
                      <td width="87" valign="top"><p align="center"><strong>Course Code</strong><br />
                              <strong>(New<u>)</u></strong></p></td>
                      <td width="307" valign="top"><p align="center"><strong>Course Title</strong></p></td>
                      <td width="55" valign="top"><p align="center"><strong>Credit Hours</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hours / Week<u></u></strong></p></td>
                      <td width="96" valign="top"><p align="center"><strong>Prerequisite Courses</strong></p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4111</p></td>
                      <td width="87" valign="top"><p>CSIT-4211</p></td>
                      <td width="307" valign="top"><p>Artificial    Intelligence with Prolog</p></td>
                      <td width="55" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4112</p></td>
                      <td width="87" valign="top"><p>CSIT-4212</p></td>
                      <td width="307" valign="top"><p>Artificial    Intelligence with Prolog: Sessional</p></td>
                      <td width="55" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4113</p></td>
                      <td width="87" valign="top"><p>CSIT-4202</p></td>
                      <td width="307" valign="top"><p>Data Communication</p></td>
                      <td width="55" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4114</p></td>
                      <td width="87" valign="top"><p>CSIT-4203</p></td>
                      <td width="307" valign="top"><p>Data Communication    Sessional</p></td>
                      <td width="55" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4121</p></td>
                      <td width="87" valign="top"><p>CSIT-4213</p></td>
                      <td width="307" valign="top"><p>Simulation &amp; Modeling </p></td>
                      <td width="55" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4122</p></td>
                      <td width="87" valign="top"><p>CSIT-4214</p></td>
                      <td width="307" valign="top"><p>Simulation &amp; Modeling:    Sessional</p></td>
                      <td width="55" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4131</p></td>
                      <td width="87" valign="top"><p>CSIT-4215</p></td>
                      <td width="307" valign="top"><p>Neural Network &amp; Fuzzy Logic</p></td>
                      <td width="55" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">CSC-3141</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4141</p></td>
                      <td width="87" valign="top"><p>CSIT-4216</p></td>
                      <td width="307" valign="top"><p>Advanced Algorithms</p></td>
                      <td width="55" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="96" valign="top"><p align="center">CSC-1232</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4151</p></td>
                      <td width="87" valign="top"><p>CSIT-4217</p></td>
                      <td width="307" valign="top"><p>Graph Theory &amp; Applications</p></td>
                      <td width="55" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="90" valign="top"><p align="center">CSC-4152</p></td>
                      <td width="87" valign="top"><p>CSIT-4218</p></td>
                      <td width="307" valign="top"><p>Graph Theory &amp; Applications:    Sessional</p></td>
                      <td width="55" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="96" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
               	  <p><strong>Option-II for Twelfth Semester </strong>(Any two courses and their Sessionals, if any, to be taken)<strong></strong></p>
               	  <p>&nbsp;</p>
               	  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="98" valign="top"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)<u></u></strong></p></td>
                      <td width="92" valign="top"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)<u></u></strong></p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course Title<u></u></strong></p></td>
                      <td width="64" valign="top"><p align="center"><strong>Credit Hours</strong></p></td>
                      <td width="65" valign="top"><p align="center"><strong>Contact Hours / Week<u></u></strong></p></td>
                      <td width="84" valign="top"><p align="center"><strong>Prerequisite Courses</strong><strong><u> </u></strong></p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4241<u></u></p></td>
                      <td width="92" valign="top"><p>CSIT-4311</p></td>
                      <td width="276" valign="top"><p>Java Programming</p></td>
                      <td width="64" valign="top"><p align="center">2</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">ECE-3011</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4242</p></td>
                      <td width="92" valign="top"><p>CSIT-4312</p></td>
                      <td width="276" valign="top"><p>Java Programming: Sessional</p></td>
                      <td width="64" valign="top"><p align="center">1</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">ECE-3012</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4251</p></td>
                      <td width="92" valign="top"><p>CSIT-4313</p></td>
                      <td width="276" valign="top"><p>Computer Security</p></td>
                      <td width="64" valign="top"><p align="center">3</p></td>
                      <td width="65" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4261</p></td>
                      <td width="92" valign="top"><p>CSIT-4314</p></td>
                      <td width="276" valign="top"><p>Multimedia Communications </p></td>
                      <td width="64" valign="top"><p align="center">3</p></td>
                      <td width="65"><p align="center">3</p></td>
                      <td width="84" valign="top"><h3>EEE-3123 </h3></td>
                    </tr>
                    <tr>
                      <td width="98"><p align="center">ECE-4027</p></td>
                      <td width="92"><p>CSIT-4301</p></td>
                      <td width="276"><p>VLSI&nbsp;I </p></td>
                      <td width="64"><p align="center">2</p></td>
                      <td width="65"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98"><p align="center">ECE-4028</p></td>
                      <td width="92"><p>CSIT-4302</p></td>
                      <td width="276"><p>VLSI&nbsp;I&nbsp;Laboratory </p></td>
                      <td width="64"><p align="center">1</p></td>
                      <td width="65"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4271</p></td>
                      <td width="92" valign="top"><p>CSIT-4315</p></td>
                      <td width="276" valign="top"><p>Advanced Operating    Systems</p></td>
                      <td width="64" valign="top"><p align="center">2</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-2131</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4272</p></td>
                      <td width="92" valign="top"><p>CSIT-4316</p></td>
                      <td width="276" valign="top"><p>Advanced Operating    Systems: Sessional</p></td>
                      <td width="64" valign="top"><p align="center">1</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-2132</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4281</p></td>
                      <td width="92" valign="top"><p>CSIT-4317</p></td>
                      <td width="276" valign="top"><p>Decision Support    Systems </p></td>
                      <td width="64" valign="top"><p align="center">2</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4282</p></td>
                      <td width="92" valign="top"><p>CSIT-4318</p></td>
                      <td width="276" valign="top"><p>Decision Support    Systems: Sessional </p></td>
                      <td width="64" valign="top"><p align="center">1</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4291</p></td>
                      <td width="92" valign="top"><p>CSIT-4303</p></td>
                      <td width="276" valign="top"><p>Robotics &amp; Computer Vision</p></td>
                      <td width="64" valign="top"><p align="center">2</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="98" valign="top"><p align="center">CSC-4292</p></td>
                      <td width="92" valign="top"><p>CSIT-4304</p></td>
                      <td width="276" valign="top"><p>Robotics &amp; Computer Vision    Sessional</p></td>
                      <td width="64" valign="top"><p align="center">1</p></td>
                      <td width="65" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
               	  <p><u>Approximate Course  Distribution</u><u> </u><br />
           	      First Semester (First Year) </p>
               	  <p>&nbsp;</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong></p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td colspan="3" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td colspan="2"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ENG-1011</p></td>
                      <td width="92" valign="top"><p>&nbsp;</p></td>
                      <td width="276" valign="top"><p>English Reading<u></u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td colspan="3" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ART-1011</p></td>
                      <td width="92" valign="top"><p>&nbsp;</p></td>
                      <td width="276" valign="top"><p>Bangladesh Studies</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td colspan="3" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">MATH-1011</p></td>
                      <td width="92" valign="top"><p>CSIT-1121</p></td>
                      <td width="276" valign="top"><p>Differential and Integral    Calculus</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td colspan="3" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-1011</p></td>
                      <td width="92" valign="top"><p>CSIT-1101</p></td>
                      <td width="276" valign="top"><p>Introduction to    Computers</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td colspan="3" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-1012<u></u></p></td>
                      <td width="92" valign="top"><p>CSIT-1102</p></td>
                      <td width="276" valign="top"><p>Introduction to Computers: Sessional<u> </u></p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td colspan="3" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A </p></td>
                    </tr>
                    <tr>
                      <td colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td colspan="2" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="112" valign="top"><p align="center"><strong>13</strong></p></td>
                      <td colspan="2" valign="top"></td>
                      <td width="126"></td>
                    </tr>
                  </table>
                  <p align="left">Second Semester (First Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="80"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong></p></td>
                      <td width="288" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ENG-1113</p></td>
                      <td width="80" valign="top"><p>&nbsp;</p></td>
                      <td width="288" valign="top"><p>English Writing<u></u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">ENG-1011</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ART-1113</p></td>
                      <td width="80" valign="top"><p>&nbsp;</p></td>
                      <td width="288" valign="top"><p>Introduction to    Sociology</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">MATH-1113</p></td>
                      <td width="80" valign="top"><p>CSIT-1221</p></td>
                      <td width="288" valign="top"><p>Linear Algebra and Matrix</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">MATH-1011 </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-1113</p></td>
                      <td width="80" valign="top"><p>CSIT-1211</p></td>
                      <td width="288" valign="top"><p>Computer Ethics and Law</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-1115</p></td>
                      <td width="80" valign="top"><p>CSIT-1212</p></td>
                      <td width="288" valign="top"><p>Discrete    Mathematics</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="456" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>14</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>14</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Third  Semester</strong> (First Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="292" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p>ENG-1215</p></td>
                      <td width="92" valign="top"><p>&nbsp;</p></td>
                      <td width="292" valign="top"><p>Spoken English</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p>MGT-1215</p></td>
                      <td width="92" valign="top"><p>&nbsp;</p></td>
                      <td width="292" valign="top"><p>Introduction to    Business</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">MATH-1215</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-1321</p></td>
                      <td width="292" valign="top"><p>Ordinary &amp;    Partial Differential Equations</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p>MATH-1113</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">PHY-1211</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-1331</p></td>
                      <td width="292" valign="top"><p>General Physics I</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">PHY-1212</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-1332</p></td>
                      <td width="292" valign="top"><p>General Physics I Laboratory</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECE-1215</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-1301</p></td>
                      <td width="292" valign="top"><p>Electronics I</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">ECE-1113</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECE-1216</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-1302</p></td>
                      <td width="292" valign="top"><p>Electronics    Circuit Simulation Laboratory</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">ECE-1114</p></td>
                    </tr>
                    <tr>
                      <td width="472" colspan="3" valign="top"><p align="center">Total </p></td>
                      <td width="72" valign="top"><p align="center">15</p></td>
                      <td width="60" valign="top"><p align="center">17</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Fourth  Semester </strong>(Second  Year)<strong></strong></p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ACC-2011</p></td>
                      <td width="92" valign="top"><p>&nbsp;</p></td>
                      <td width="276" valign="top"><p>Principles of Accounting</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">MATH-2011</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2121</p></td>
                      <td width="276" valign="top"><p>Co-0rdinate    Geometry and Vector Analysis </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">MATH-1215 </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2011</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2101</p></td>
                      <td width="276" valign="top"><p>Computer Programming</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1011</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2012</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2102</p></td>
                      <td width="276" valign="top"><p>Computer    Programming: Laboratory</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1012</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CE-2010</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2103</p></td>
                      <td width="276" valign="top"><p>Engineering    Drawing</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="456" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>13</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Fifth  Semester </strong>(Second  Year)<strong></strong></p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="79"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong></p></td>
                      <td width="289" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td colspan="2" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td colspan="2"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECON-2111</p></td>
                      <td width="79" valign="top"><p><u>&nbsp;</u></p></td>
                      <td width="289" valign="top"><p>Principles of Economics<u> </u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="center">MATH-2113</p></td>
                      <td width="79"><p>&nbsp;</p></td>
                      <td width="289"><p>Probability and    Statistics </p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td colspan="2" valign="top"><p align="center">3</p></td>
                      <td colspan="2"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">PHY-2113</p></td>
                      <td width="79" valign="top"><p align="center">CSIT-2231</p></td>
                      <td width="289" valign="top"><p>General Physics II<u></u></p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">PHY-1110</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">PHY-2114</p></td>
                      <td width="79" valign="top"><p align="center">CSIT-2232</p></td>
                      <td width="289" valign="top"><p>General Physics: Sessional<u></u></p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td colspan="2" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">PHY-1111</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2113</p></td>
                      <td width="79" valign="top"><p align="center">CSIT-2201</p></td>
                      <td width="289" valign="top"><p>Numerical Methods</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">CSC-1120</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2114</p></td>
                      <td width="79" valign="top"><p align="center">CSIT-2202</p></td>
                      <td width="289" valign="top"><p>Numerical Methods: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td colspan="2" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECE-2111</p></td>
                      <td width="79" valign="top"><p align="center">CSIT-2211</p></td>
                      <td width="289" valign="top"><p>Semiconductor    Devices</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">2</p></td>
                      <td colspan="2" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>14</strong></p></td>
                      <td width="111" valign="top"><p align="center"><strong>16</strong></p></td>
                      <td colspan="2" valign="top"><p>&nbsp;</p></td>
                      <td width="121"><p>&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong> Sixth Semester </strong>(Second Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong></p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="center">CHEM-2211</p></td>
                      <td width="92"><p align="center">CSIT-2351</p></td>
                      <td width="276"><p>Chemistry</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="center">CHEM-2212</p></td>
                      <td width="92"><p align="center">CSIT-2352</p></td>
                      <td width="276"><p>Chemistry Laboratory </p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="center">ME-1011</p></td>
                      <td width="92"><p align="center">CSIT-2301</p></td>
                      <td width="276"><p>Mechanical Engineering Fundamentals</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="center">ME-1012</p></td>
                      <td width="92"><p align="center">CSIT-2302</p></td>
                      <td width="276"><p>Mechanical Engineering Fundamentals    Laboratory </p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2211</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2311</p></td>
                      <td width="276" valign="top"><p>Operating System</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1011</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2212</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2312</p></td>
                      <td width="276" valign="top"><p>Operating System:    Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1012</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2213</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2313</p></td>
                      <td width="276" valign="top"><p>Data Structures    &amp; Algorithms</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-2214</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-2314</p></td>
                      <td width="276" valign="top"><p>Data Structures &amp; Algorithms:    Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1122</p></td>
                    </tr>
                    <tr>
                      <td width="456" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>13</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>17</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Seventh  Semester</strong> (Third Year) </p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="99"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="282" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3011</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3101</p></td>
                      <td width="282" valign="top"><p>Object Oriented    Programming</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1121</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3012</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3102</p></td>
                      <td width="282" valign="top"><p>Object Oriented    Programming: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1122</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3013</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3111</p></td>
                      <td width="282" valign="top"><p>Data Warehousing &amp; Data    Mining</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3015</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3112</p></td>
                      <td width="282" valign="top"><p>Open Source Tools &amp;    Techniques</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-2241</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3016</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3113</p></td>
                      <td width="282" valign="top"><p>Open Source Tools &amp;    Techniques: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-2242</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3017</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3114</p></td>
                      <td width="282" valign="top"><p>Database    Management System <u></u></p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1231</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3018</p></td>
                      <td width="99" valign="top"><p align="center">CSIT-3115</p></td>
                      <td width="282" valign="top"><p>Database Management System:    Sessional <u></u></p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-1232</p></td>
                    </tr>
                    <tr>
                      <td width="469" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>15</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Eighth  Semester</strong> (Third Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="85"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="283" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="50" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td colspan="3"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3113</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3201</p></td>
                      <td width="283" valign="top"><p>Computer    Networking</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="50" valign="top"><p align="center">2</p></td>
                      <td colspan="3" valign="top"><p align="center">CSC-2132 &amp;</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3114</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3202</p></td>
                      <td width="283" valign="top"><p>Computer    Networking: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="50" valign="top"><p align="center">2</p></td>
                      <td colspan="3" valign="top"><p align="center">CSC-2133 &amp;</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3115</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3211</p></td>
                      <td width="283" valign="top"><p>Computer    Architecture &amp; Design</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="50" valign="top"><p align="center">3</p></td>
                      <td colspan="3" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3117</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3212</p></td>
                      <td width="283" valign="top"><p>Distributed    Systems</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="50" valign="top"><p align="center">2</p></td>
                      <td colspan="3" valign="top"><p align="center">CSC-3141</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3118</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3213</p></td>
                      <td width="283" valign="top"><p>Distributed    Systems: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="50" valign="top"><p align="center">2</p></td>
                      <td colspan="3" valign="top"><p align="center">CSC-3142</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECE-3121</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3203</p></td>
                      <td width="283" valign="top"><p>Microprocessor, Assembly Language    &amp; Computer Interfacing</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="50" valign="top"><p align="center">3</p></td>
                      <td colspan="3" valign="top"><p align="center">ECE-2113</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">ECE-3122</p></td>
                      <td width="85" valign="top"><p align="center">CSIT-3204</p></td>
                      <td width="283" valign="top"><p>Microprocessor, Assembly Language    &amp; Computer Interfacing: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="50" valign="top"><p>2</p></td>
                      <td colspan="3" valign="top"><p align="center">ECE-2114</p></td>
                    </tr>
                    <tr>
                      <td colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>13</strong></p></td>
                      <td colspan="2" valign="top"><p align="center"><strong>16</strong></p></td>
                      <td width="3" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="118"><p>&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Ninth  Semester </strong>(Third  Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="108"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="292" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3213</p></td>
                      <td width="108" valign="top"><p align="center">CSIT-3311</p></td>
                      <td width="292" valign="top"><p>Software    Engineering</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-2131</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3215</p></td>
                      <td width="108" valign="top"><p align="center">CSIT-3301</p></td>
                      <td width="292" valign="top"><p>Internet, Intranet    &amp; Extranet Applications</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-3141 </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3216</p></td>
                      <td width="108" valign="top"><p align="center">CSIT-3302</p></td>
                      <td width="292" valign="top"><p>Internet, Intranet    &amp; Extranet Applications Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-3142 </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-3217</p></td>
                      <td width="108" valign="top"><p align="center">CSIT-3312</p></td>
                      <td width="292" valign="top"><p>Theory of    Computation</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="right">ECE-3227 </p></td>
                      <td width="108"><p align="center">CSIT-3303</p></td>
                      <td width="292"><p>Control System I </p></td>
                      <td width="72"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">ECE-3013 </p></td>
                    </tr>
                    <tr>
                      <td width="88"><p align="right">ECE-3228 </p></td>
                      <td width="108"><p align="center">CSIT-3304</p></td>
                      <td width="292"><p>Control    System&nbsp;I&nbsp;Laboratory </p></td>
                      <td width="72"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84"><p align="center">ECE-3014 </p></td>
                    </tr>
                    <tr>
                      <td width="488" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>10</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Tenth  Semester </strong>(Fourth  Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4011</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4111</p></td>
                      <td width="276" valign="top"><p>Object Oriented    Software Development Using UML</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-3131</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4012</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4112</p></td>
                      <td width="276" valign="top"><p>Object Oriented    Software Development Using UML: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">CSC-3131</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4015</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4113</p></td>
                      <td width="276" valign="top"><p>Management    Information System</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">3</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4017</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4114</p></td>
                      <td width="276" valign="top"><p>System Analysis,    Design and Development</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4018</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4115</p></td>
                      <td width="276" valign="top"><p>System Analysis,    Design and Development: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4115</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4116</p></td>
                      <td width="276" valign="top"><p>Compiler Design    &amp; Construction</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4116</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4117</p></td>
                      <td width="276" valign="top"><p>Compiler Design    &amp; Construction: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="456" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>15</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p><strong>Eleventh  Semester </strong>(Fourth  Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="92"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="276" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="60" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4115</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4116</p></td>
                      <td width="276" valign="top"><p>Compiler Design    &amp; Construction</p></td>
                      <td width="72" valign="top"><p align="center">2</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4116</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4117</p></td>
                      <td width="276" valign="top"><p>Compiler Design    &amp; Construction: Sessional</p></td>
                      <td width="72" valign="top"><p align="center">1</p></td>
                      <td width="60" valign="top"><p align="center">2</p></td>
                      <td width="84" valign="top"><p align="center">N/A</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">Specialized<br />
                        Course<u></u></p></td>
                      <td width="92" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="276" valign="top"><p>Option I Elective Course (One    Course and Sessional, if any)</p></td>
                      <td width="72" valign="top"><p align="center">3</p></td>
                      <td width="60" valign="top"><p align="center">4</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">CSC-4000</p></td>
                      <td width="92" valign="top"><p align="center">CSIT-4201</p></td>
                      <td width="276" valign="top"><p>Project / Thesis    (Supervisor based)</p></td>
                      <td width="72" valign="top"><p align="center">6</p></td>
                      <td width="60" valign="top"><p align="center">12</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="456" colspan="3" valign="top"><p align="center"><strong>Total</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>12</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>18</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <p class="txt"><strong>Twelfth  Semester </strong>(Fourth  Year)</p>
                  <table border="1" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="88"><p align="center"><strong>Course No.</strong><br />
                              <strong>(Old)</strong> </p></td>
                      <td width="296"><p align="center"><strong>Course No.</strong><br />
                              <strong>(New)</strong> </p></td>
                      <td width="72" valign="top"><p align="center"><strong>Course    Title</strong><strong> </strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>Credit Hours </strong> </p></td>
                      <td width="84" valign="top"><p align="center"><strong>Contact Hour/ Week</strong> </p></td>
                      <td width="84"><p align="center"><strong>Prerequisite Courses</strong> </p></td>
                    </tr>
                    <tr>
                      <td width="88" valign="top"><p align="center">Specialized<br />
                        Courses<u></u></p></td>
                      <td width="296" valign="top"><p>Option II Elective Courses (Two    Courses and Sessionals, if any)</p></td>
                      <td width="72" valign="top"><p align="center">6</p></td>
                      <td width="60" valign="top"><p align="center">8</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                    <tr>
                      <td width="384" colspan="2" valign="top"><p align="center"><strong>Total</strong></p></td>
                      <td width="72" valign="top"><p align="center"><strong>6</strong></p></td>
                      <td width="60" valign="top"><p align="center"><strong>8</strong></p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                      <td width="84" valign="top"><p align="center">&nbsp;</p></td>
                    </tr>
                  </table>
                  <div class="clear"></div>
                    <div class="sec3"></div>
                    <div class="sec5"> </div>
              </div>
            </div>&nbsp;</p></td></tr>
      <p>&nbsp;</p>
      <p align="justify">&nbsp;</p>
      </table>
              </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
    <div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="#">Academic Calendar</a></li>
              <li><a href="">Admissions 2013</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#" target="_blank">Student Portal</a></li>
              <li><a href="#" target="_blank">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i>" Committed to Academic Excellence  "</i></p>
            <em class="shoutright">&nbsp;</em> </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:+880 1824455500</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>&copy; 2013 Southern University Bangladesh - All Rights Reserved -</br><b>Developed by SUB IT-Department</b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</div></body>
</html>