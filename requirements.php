
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Admission Requirements</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>   <!--Menu Area -->
    <div id="nav">
             <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Admission Requirements</h2>
            <ul>
              <li>
                <div class="desc1">
<p align="justify">Southern University Bangladesh operates its program on tri-semester bases. There will be three semesters in an academic year: Spring, Summer and Fall.</p><br /><br />

<b>Admission Requirements</b><br /><br />

Acceptable scores at Southern Admission Test (SAT) or at least 550 in (TOEFL).<br /><br />
<p align="justify">New students are admitted throughout the year, for spring, summer and fall semesters. Please check for the last date of submitting application in a particular semester. All admission queries should be directed to the Admission. Application forms are available @ Tk. 500/=Only completed applications with all certificates, transcripts, mark sheets, letters of recommendation and photographs, etc. of the students will be processed.</p><br /><br />

<b>Admission: Undergraduate</b><br /><br />

<p align="justify">Minimum 12 (Twelve) years of schooling prior to undergraduate program or equivalents.  (2.5/5.0 GPA) in the examinations.</p><br /><br />

<b>Admission Graduate</b><br /><br />

To enter into the Masters Program, one must have the following:<br /><br />

<p align="justify">Minimum 15 (Fifteen) years of schooling prior to Masters. <b>OR</b> 4 (Four) years Bachelors Degree (or 3 years Bachelor plus 1 year Master degree).</p><br /><br />

Relative 3-5 years working experience is to be considered as an added advantage to enroll into Graduate Program.<br /><br />

<center><b>"Only self-motivated persons are encouraged to apply in Executive program"</b></center><br /><br />

<b>Admission Diploma /Certificate</b><br /><br />

<p align="justify">There are no fixed criteria in these programs. The Admission Committee looks for individuals who want to learn in any suitable course(s). Students who want to learn are encouraged to enroll.</p><br /><br />

<b>Course Enrollment Dates</b><br /><br />

<p align="justify">Students approved for admission may enroll in on-campus courses any time prior to and including the first two weeks of class for each semester.</p>
<br />
  <br />
  <br />

  <b>Programs</b><br />
  <br />
<p align="justify">Undergraduate Programs are designed to graduate business ,legal, and technology professionals directed toward employment in related areas. A maximum of 190 semester hours and a minimum of 120 semester hours are required depending upon the academic background in English and Math of the student. All degree have unique requirements, students should know all regulations and the particular requirements for each degree.</p><br /><br />
 
<b>Undergraduate Admission</b><br /><br />

To enter in to the Bachelors Program of at Southern University Bangladesh one must have the following:<br /><br />
<p align="justify">Twelve years of education prior to undergraduate program. or from Madrasha Board or five subjects from O-level and two major subjects in A-level education are required for admission into Southern university or equivalent to Twelve years of education.</p><br /><br />

<p align="justify">At least a second division or minimum GPA 2.5 out of 5.0 GPA in the S.S.C and H.S.C. examination, A student is considered to be a full time student if s/he continuously enrolled for at least two semesters per academic year. Minimum of 12 credit hours are considered a full undergraduate load in one academic semester.</p>
                
             
                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>
