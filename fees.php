
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Tuition &amp; Other Fees</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>    <!--Menu Area -->
    <div id="nav">
            <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>Tuition & Other Fees</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Financial Information</h2>
            <ul>
              <li>
                <div class="desc1">
<p align="justify">Southern University follows non-discriminative policy for tuition and fees. A foreign or out of state student pays the same tuition and fees like the local students. They could also enjoy scholarships, grants and other financial aids from the university based on financial need and merit. Followings are the charges in Bangladesh currency called Taka (BDT).</p><br />
<b>Tuition Fees & <a href="other_fees.php">Other Fees</a></b><br /><br />
<table width="100%" border="0" cellpadding="5" cellspacing="5">
  <tr>
    <td width="24%" bgcolor="#333333"><font color="#FFFFFF"><strong>Program</strong></font></td>
    <td width="25%" bgcolor="#333333"><font color="#FFFFFF"><strong>Qualifications</strong></font></td>
    <td width="16%" bgcolor="#333333"><font color="#FFFFFF"><strong>Credits</strong></font></td>
    <td width="16%" bgcolor="#333333"><font color="#FFFFFF"><strong> Duration</strong></font></td>
    <td width="19%" bgcolor="#333333"><font color="#FFFFFF"><strong>Costs(Tk)</strong></font></td>
  </tr>
</table>
<table width="100%" border="0" cellpadding="5" cellspacing="5">
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td width="24%"><strong>Executive MBA</strong></td>
    <td width="25%">Bachelor Degree &amp; work Experience</td>
    <td width="16%">42 Credits</td>
    <td width="16%"> 12 Months</td>
    <td width="19%"><strong>75,000</strong></td>
  </tr>
  <tr>
    <td><strong>MBA</strong></td>
    <td>4 Years Business Graduates</td>
    <td>48 Credits</td>
    <td> 12 Months</td>
    <td><strong>68,300</strong></td>
  </tr>
  <tr>
    <td><strong>MBA</strong></td>
    <td>Bachelor Degree</td>
    <td>63 Credits</td>
    <td> 20 Months</td>
    <td><strong>1,00,000</strong></td>
  </tr>
  <tr>
    <td><strong>MS in CSIT</strong></td>
    <td>Bachelor Dgree</td>
    <td>60 Credits</td>
    <td> 20 Months</td>
    <td><strong>1,02,600</strong></td>
  </tr>
  <tr>
    <td><strong>MS in CSIT</strong></td>
    <td>4 Years IT / CS Graduate</td>
    <td>39 Credits</td>
    <td> 12 Months</td>
    <td><strong>66,690</strong></td>
  </tr>
  <tr>
    <td><strong>LLM</strong></td>
    <td>4 Years LLB</td>
    <td>30 Credit</td>
    <td> 12 Months</td>
    <td><strong>30,000</strong></td>
  </tr>
  <tr>
    <td><strong>LLM</strong></td>
    <td>Bachelor Degree</td>
    <td>48 Credit</td>
    <td> 20 Months</td>
    <td><strong>50,000</strong></td>
  </tr>
  <tr>
    <td><strong>MA in English</strong></td>
    <td>4 Years Bachelor in English</td>
    <td>30 -Credit</td>
    <td> 12 Months</td>
    <td><strong>30,000</strong></td>
  </tr>
  <tr>
    <td><strong>MA in English</strong></td>
    <td>Bachelor Degree</td>
    <td>60 Credit</td>
    <td> 20 Months</td>
    <td><strong>50,000</strong></td>
  </tr>
  <tr>
    <td><strong>MPharm</strong></td>
    <td>BPharm</td>
    <td>36 Credit</td>
    <td> 1 Months</td>
    <td><strong>85,000</strong></td>
  </tr>
  <tr>
    <td><strong>MPharm (Non Thesis)</strong></td>
    <td>BPharm</td>
    <td>30 Credit</td>
    <td> 1 Months</td>
    <td><strong>80,000</strong></td>
  </tr>
  <tr>
    <td><strong>MA in Islamic Studies</strong></td>
    <td>Bachelor Degree</td>
    <td>60 Credit</td>
    <td> 20 Months</td>
    <td><strong>35,000</strong></td>
  </tr>
   <tr>
    <td><strong>MA in Islamic Studies</strong></td>
    <td>Bachelor Degree</td>
    <td>30 Credit</td>
    <td> 12 Months</td>
    <td><strong>25,000</strong></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td><strong>LLB (2 years)</strong></td>
    <td>Bachelor Degree</td>
    <td>60 Credits</td>
    <td> 20 Months</td>
    <td><strong>50,000</strong></td>
  </tr>
  <tr>
    <td colspan="5">&nbsp;</td>
  </tr>
  <tr>
    <td><strong>BBA</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>129 Credits</td>
    <td> 4 Years </td>
    <td><strong>1,78,000</strong></td>
  </tr>
  <tr>
    <td><strong>BSc in CSIT</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>142 Credits</td>
    <td> 4 Years<strong>,</strong> 12 semesters </td>
    <td><strong>2,34,300</strong></td>
  </tr>
  <tr>
    <td><strong>BSc in CSIT</strong></td>
    <td>Diploma Holder </td>
    <td>107 Credits</td>
    <td> 2 Years 8 Month<strong>,</strong> 8 semesters </td>
    <td><strong>1,76,550</strong></td>
  </tr>
  
    <tr>
    <td><strong>BSc in CSE</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>142 Credits</td>
    <td> 4 Years<strong>,</strong> 12 semesters </td>
    <td><strong>2,34,300</strong></td>
  </tr>
  
  <tr>
    <td><strong>LLB 4 Years</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>123 Credits</td>
    <td>4 Years, 12 semesters</td>
    <td><strong>1,40,000</strong></td>
  </tr>
  <tr>
    <td><strong>BA in English</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>120 Credits</td>
    <td>4 Years, 12 semesters</td>
    <td><strong>1,25,000</strong></td>
  </tr>
  <tr>
    <td><strong>BSc in EEE</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>160 Credits</td>
    <td> 4 Years<strong>,</strong> 12 semesters </td>
    <td><strong>2,80,000</strong></td>
  </tr>
   <tr>
    <td><strong>BSc in ECE</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>160 Credits</td>
    <td> 4 Years<strong>,</strong> 12 semesters </td>
    <td><strong>2,80,000</strong></td>
  </tr>
    <tr>
    <td><strong>BSc in EEE</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>112 Credits</td>
    <td> 2Years<strong>,</strong> 8 semesters </td>
    <td><strong>1,96,000</strong></td>
  </tr>
    <tr>
    <td><strong>BSc in ECE</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>115 Credits</td>
    <td> 2Years<strong>,</strong> 8 semesters </td>
    <td><strong>2,0,1250</strong></td>
  </tr>
      <tr>
    <td><strong>BSc in Civil</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>162 Credits</td>
    <td> 4 Years<strong>,</strong> 8 semesters </td>
    <td><strong>2,77,830</strong></td>
  </tr>
  <tr>
    <td><strong>BSc in Civil</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>142 Credits</td>
    <td> 3.5 Years<strong>,</strong> 7 semesters </td>
    <td><strong>2,43,530</strong></td>
  </tr>
  
       <tr>
    <td><strong>Diploma in Civil</strong></td>
    <td>SSC</td>
    <td>120 Credits</td>
    <td> 4 Years<strong>,</strong> 8 semesters </td>
    <td><strong>n/a</strong></td>
  </tr>
  
    <tr>
    <td><strong>BPharm</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>162 Credits</td>
    <td>4 Years, 12 semesters</td>
    <td><strong>2,77,830</strong></td>
  </tr>
  <tr>
    <td><strong>Hotel &amp;Tourism</strong>    <strong>Management</strong></td>
    <td>12 years school/College Geaduate</td>
    <td>126 Credits</td>
    <td> 4 Years, 12 semesters </td>
    <td><strong>2,74,750</strong></td>
  </tr>
</table>
</br>
 <P align="center"><a href="other_fees.php">OTHER FEES</a></p>            
                </div>
              </li>
             </ul>
          </div>
  
        </div>
           <br class="clear" />
        </div>
      </div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>