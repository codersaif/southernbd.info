
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Member of Trustee Board</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
.style1 {
	font-size: 18px;
	color: #000066;
}
.style2 {
	font-size: 18px;
	font-weight: bold;
	font-style: italic;
}
-->
</style>
</head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>    <!--Menu Area -->
    <div id="nav">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   
    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Department of Business Administration</h2>
            <p align="center" class="style1">&nbsp;</p>
            <ul><li>
              <div class="desc1">
<table width="100%" cellpadding="0" cellspacing="0" border="0" align="center">
<tr>
	<td>&nbsp;</td>
    <td><br />
         <p align="center" class="style2"> Southern University Bangladesh</p>
      <ul>
      </ul>      
      <p>&nbsp;</p>
      <p align="justify"><b>BBA (Bachelor of Business Administration)</b><br />
          <br />
        The Department of Business Administration at the Southern University Bangladesh brings together various business disciplines to nurture a diverse and supportive learning community. We conduct rigorous and innovative interdisciplinary research that improves theory and practice in contemporary business. We offer an educational environment that encourages life-long learning for our students, faculty, staff, and business partners. We combine the faculty of Business's strengths with those of engineering, agriculture, health and social sciences to utilize our University’s distinctive intellectual excellence and develop a new spectrum of solutions for contemporary management problems.<br />
  <br />
        Bachelor of Business Administration (BBA) is four years degree providing quality business education at undergraduate levels. Course work features practices, as well enable students to work effectively in business environment. The Bachelor of Business Administration (BBA) degree requires 126 semester hours including of 36 semester credit hours of college-level general education. </p>
      <p align="justify"> Foundation: for selective students<br />
        GE 1002 Career Planning &amp; Development<br />
        EN 1002 English Reading<br />
        CS 1000 Introduction to Computer<br />
        MA 1010 General Math<br />
        AC 1003 Fundamentals of Accounting<br />
        A. General Education (36 Semester Credits)<br />
        BS 1010 Bangladesh Studies<br />
        IR 1010 International Relations<br />
        SO 1010 Introduction to Sociology<br />
        EN 1010 English Composition<br />
        EN 1210 Spoken English<br />
        HU 1010 Basics of Humanities<br />
        ES 1010 Environmental Science<br />
        CS 1000 Computer Fundamentals &amp; Application<br />
        CS 2200 Health Science Management<br />
        MA 1020 Introduction to Statistics<br />
        MA 2060 Business Math<br />
        MA 2010 Business Calculus<br />
        B. Core Education (72 Semester Credits)<br />
        MG 2010 Introduction to Business<br />
        AC 1010 Accounting I<br />
        AC 2010 Accounting II<br />
        EC 2010 Macro Economics<br />
        EC 2020 Micro Economics<br />
        MA 2030 Inferential Statistics<br />
        MK 2050 Introduction to Marketing<br />
        MG 2060 Principles of Management<br />
        MG 3110 Business Law<br />
        MG 3210 Company and Industrial Law<br />
        FA 3220 Fundamental of Taxation<br />
        AC 3230 Theory &amp; Practice of Auditing<br />
        MG 4080 Business Communication<br />
        QM 4100 Quantitative Methods<br />
        MG 4110 Entrepreneurship Development<br />
        FA 4160 Financial Management<br />
        MG 4180 Production Management<br />
        MG 4130 Export &amp; Import Management<br />
        MG 4280 Organizational Behavior<br />
        MG 4300 Business Ethics &amp; Morality<br />
        MG 4420 Business Policy and Strategy<br />
        FA 4480 Money and Banking<br />
        MG 4510 Introduction to Business Research<br />
        C. Electives (12 Semester Credits)<br />
        Marketing (12 Credits)<br />
        MK 4510 Agricultural &amp; Industrial Marketing<br />
        MK 4600 Advertising &amp; Promotion Management<br />
        MK 4650 Sales &amp; Distribution Management<br />
        MK 4260 Introduction to Marketing Research<br />
        Finance (12 Credits)<br />
        FA 4550 Small Business Finance<br />
        FA 4600 Portfolio Management<br />
        FA 4650 Insurance &amp; Risk Management<br />
        FA 4320 Financial Reporting<br />
        Accounting(12 Credits)<br />
        AC 4510 Corporate Accounting<br />
        AC 4600 Cost Accounting<br />
        AC 4710 Macro Accounting<br />
        AC 4510 Government Accounting<br />
        Management (12 Credits)<br />
        MG 4550 Total Quality Management<br />
        MG 4560 Management Control System<br />
        MG 4570 Supply Chain Management<br />
        MG 4580 Comparative Management<br />
        Management Information System (12 Credits)<br />
  <br />
        Pre-request may required<br />
        CS-3270 Management Information Systems<br />
        CS 2060 Database Fundamentals<br />
        EC 4170 Data Communication (CS 3130 required)<br />
        IT 4190 Web Site Development<br />
        D. Internship / Project<br />
  <br />
        4980 Report &amp; Presentation (6 Semester Credits)<br />
        Optional: MS 1010 Military Science<br />
        GRADUATE PROGRAMS<br />
  <br />
        <strong>Executive Master Of Business Administration (EMBA)</strong><br />
  <br />
        Core &amp; Elective Courses: 39 semester credit hours<br />
        All Courses are three (3) Credit hour, unless it is mentioned.<br />
        Executive MBA is designed to train and develop professionals. This One-year program emphasizes business strategies and techniques in the global environment with flexibility and convenience in mind, allowing the student to choose among different concentrations. Self-motivated persons are encouraged to apply in Executive program. The Executive Master of Business Administration (EMBA) program is a 3 semesters, 39 credit hour program for professionals who have earned a four (4) year bachelor degree from a recognized college or university and has substantial work experience; Students may choose a concentration area upon completion of the required core courses. The student will also complete a capstone project with seminar for this degree program.<br />
  <br />
        Curriculum<br />
  <br />
        The Executive Master of Business Administration (EMBA) program is a 3 semesters, 39 credit hour program for professionals who have earned a four (4) year bachelor degree from a recognized college or university and has substantial work experience; Students may choose a concentration area upon completion of the required core courses. The student will also complete either a thesis or a capstone project for this degree program.<br />
  <br />
  <b>A. Foundation</b ><br />
        FA-6030, Accounting for Executives<br />
        B. Core Courses (30 cr/hrs.)<br />
        MG 5010 Management Information Systems<br />
        MG 6010 Human Resource Management<br />
        MG 6040 Project Management<br />
        FA 6040 Financial Management<br />
        MG 6040 International Management<br />
        MK 6220 Marketing Management<br />
        MG 6030 Managerial Accounting<br />
        QM 6000 Quantitative Methods for Executives<br />
        MG 6090 Research Methodology?<br />
        MG 6020 Business Policy and Strategy<br />
  <b>C. Electives (6 Cr.Hr)</b><br />
  <br />
        Marketing (6 Cr.Hr)<br />
  <br />
        MK-6050, International Marketing<br />
        MK-6060, Brand Marketing<br />
        IT 4040 E-Commerce<br />
        MK 6050 Consumer Behavior<br />
  <b>Accounting &amp; Finance (6 Cr.Hr)</b><br />
  <br />
        FA-6050, Multinational Corporate Finance<br />
        FA-6060, Investment &amp; Credit Management<br />
        AC-6080, Accounting Information System<br />
        FA 6070 Financial Analysis &amp; Reporting<br />
        MIS (Pre-requisite Required) (6 Cr.Hr)<br />
        CS 3250 System Design &amp; Development<br />
        CS 3140, Computer Network<br />
        CSC-2240, Database Management<br />
        HRM (6 Cr.Hr)<br />
        MG-6050, Organization Development and change<br />
        MG-6060, Leadership Behavior &amp; Motivation<br />
        MG 6070 Industrial Relations<br />
        MG 6040 Management Development<br />
        International Business (6 Cr.Hr)<br />
        MG-6050 International Business<br />
        FA 6040 International Finance<br />
        MK-6050 International Marketing<br />
        IT 4040 E-Commerce<br />
        D. 6980 MBA Thesis &amp; Presentation (6 Cr.Hr)<br />
  <b>Program: Masters of Business Administration (MBA</b>)<br />
  <br />
        The MBA program is designed to meet the growing needs of mid-career managers. This program provides for areas of concentration. Students are to gain an understanding of the essential managerial skills and then to focus on a particular area of concentration. This innovative concept in graduate level education allows the student to develop both a comprehensive background in basic management skills, at the same time to select course work that is aimed towards a particular career path.<br />
        Core &amp; Elective Courses of 60 semester credit hours for non-business graduates.<br />
        Core &amp; Elective Courses of 36 semester credit hours for business graduates.<br />
        Core &amp; Elective Courses 60 semester credit hours MBA/ consisting of courses Core courses and choice of specialized areas for non-business graduates. The Master of Business Administration (MBA) program is a 3 semester, 36 credit hour programs for those students who have earned a four (4) year bachelor degree from a recognized college or university; Students may choose a concentration area upon completion of the required core courses. The student will be required complete a capstone project with seminar for the MBA program.<br />
        All Courses are three (3) Credit hours unless it is mentioned.<br />
        Core &amp; Elective Courses 60 semester credit hours for non-business graduates<br />
  <br />
        A. Core Courses (45 Cr.Hr)<br />
  <br />
        FA-5010 Accounting For Executives<br />
        MA 2060 Business Mathematics<br />
        MG 5100 Business Statistics<br />
        MG 5010 Management Information Systems<br />
        MG 5020 Business Communication<br />
        MG 6010 Human Resource Management<br />
        MK-6220 Marketing Management<br />
        QM-6000 Quantitative Methods for Executive<br />
        AC 6030 Managerial Accounting<br />
        MG 6040 International Management<br />
        MG 6050 Project Management<br />
        MG-6090 Research Methodology<br />
        MG 6020 Business Policy &amp; Strategy<br />
        B. Elective Courses<br />
  <b>Marketing</b><br />
  <br />
        MK-6060 Brand Marketing<br />
        MK-6050 International Marketing<br />
        MK 6050 Consumer Behavior<br />
        MK-6080? Services Marketing<br />
        Accounting and Finance (9 Cr.Hr)<br />
        FA-6050 Multinational Corporate Finance<br />
        FA-6060 Investment &amp; Credit Management<br />
        FA-6070Cost Accounting<br />
  <br />
        AC-6080 Accounting Information System<br />
        Human Resource Management(9 Cr.Hr)<br />
        MG-6050, Organization Development &amp; change<br />
        MG-6060, Leadership Behavior &amp; Motivation<br />
        MG 6070 Industrial Relations<br />
        MG 60 Company, labor &amp; Industrial law<br />
        International Business (9 Cr.Hr)<br />
        MG 6050 International Business<br />
        FA 6040 International Finace<br />
        MK-6050 International Marketing<br />
        IT 6040 E-Commerce<br />
        MIS(9 Cr.Hr)<br />
  <br />
        CS 3250 System Design &amp; Development<br />
        CS 3140, Computer Network<br />
        CSC-2240, Database Management<br />
        IT 4040 E-Commerce<br />
        Bank Management (9 Cr.Hr)<br />
  <br />
        BK 6010 Banking Operations Management<br />
        FA 6060 Investment &amp; Credit Management<br />
        MK 4700 Managing Customer Services<br />
        BK 6060 Islamic Banking system<br />
        C. 6980 MBA Thesis/Capstone Project (6 Cr.Hr)<br />
  <b>Program: Masters of Business Administration (MBA)</b><br />
  <br />
        All Courses are three (3) Credit hours unless it is mentioned.<br />
        Core &amp; Elective Courses 36 semester credit hours for business graduates<br />
        FA 6040 Financial (International) Management<br />
        MG 6010 Human Resource Management<br />
        MK 6220 Marketing Management<br />
        MG 6030 Managerial Accounting<br />
        MG 6110 Project Management<br />
        EC 6050 Managerial Economics<br />
        Elective Courses(12 cr/hrs.)<br />
  <br />
  <b>Marketing</b><br />
  <br />
        MK 6060 Brand Marketing<br />
        MK 6050 International Marketing<br />
        MK 6080 Services Marketing<br />
        Accounting and Finance<br />
        FA 6050 Multinational Corporate Finance<br />
        FA 6060 Investment &amp; Credit Management<br />
        FA 6070 Cost Accounting<br />
        AC 6080 Accounting Information System<br />
        Human Resource Management<br />
        MG 6050 Organization Development<br />
        MG 6060 Leadership Behavior &amp; Motivation<br />
        MG 6070 Industrial Relations<br />
        MG 6040 Management Development<br />
        International Business<br />
        MG 6050 International Business<br />
        FA 6040 International Finance<br />
        MK 6050 International Marketing<br />
        IT 6040 E-Commerce<br />
  <br />
       <b> MIS</b><br />
  <br />
        CS 3250 System Design &amp; Development<br />
        CS 3140 Computer Network<br />
        CSC-2240 Database Management<br />
        IT 4040 E-Commerce<br />
        Bank Management<br />
        ations Management<br />
        FA 6060 Investment &amp; Credit Management<br />
        MK 4700 Managing Customer Services<br />
        BK 6060 Islamic Banking system<br />
        BK 6020 Development Banking Institutions<br />
        MBA Dissertation/Capstone Project (6 Cr.Hr)<br />
      </p>
      <p>&nbsp;</p>
</table>
              </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>
</div></body>
</html>