
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Faculty Members</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
.style3 {
	font-weight: bold;
	font-size: 24px;
	color: #0000FF;
}
.style4 {
	font-size: 18px;
	color: #00FF00;
}
.style6 {font-size: 18px; color: #00FF00; font-weight: bold; }
.style7 {font-size: 16px}
body,td,th {
	font-size: 16px;
}
.style8 {color: #00CC00}
.style9 {
	color: #00FF00;
	font-weight: bold;
}
.style10 {
	font-size: 24px;
	color: #000099;
	font-weight: bold;
}
.style11 {color: #00CC00; font-weight: bold; }
-->
</style>
</head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>   <!--Menu Area -->
    <div id="nav">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


     </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>

</div>
    <p><br class="clear" /><!--footer -->
    </p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3"><u>Faculty Members</u> </p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3">&nbsp;</p>
    <p align="center" class="style3"><u>Faculties of Business Administration</u></p>
    <p align="center" class="style3">&nbsp;</p>
    <div align="center">
      <table width="975" border="1" align="center">
        <tr>
          <td width="231"><div align="center"><span class="style4">Name</span></div></td>
          <td width="345"><div align="center"><span class="style4">Designation</span></div></td>
          <td width="568"><div align="center"><span class="style6">Universties</span></div></td>
        </tr>
        <tr>
          <td><span class="style7">Prof. Ahmad Golam Sorwar </span></td>
          <td><span class="style7">Registrar</span></td>
          <td><p>MBA </p>
            <p>B. Com (Hons.)M. Com(Management)</p></td>
        </tr>
        <tr>
          <td>Md. Riduanul Hoque</td>
          <td>Senior Lecturer &amp; Coordinator </td>
          <td>BBA-IIUC(Finance), MBA-USTC(Finance)</td>
        </tr>
        <tr>
          <td>Md.Kutub Uddin</td> 
          <td>Senior Lecturer &amp; coordinator </td>
          <td><p>BBA- University Campus suffolk, UK awarded by University of suffolk, &amp; University of Essex &amp; University of East Anglia. </p>
            <p>MBA - UK(Management)</p></td>
        </tr>
        <tr>
          <td>Kamrul Hasan </td>
          <td>Senior Lecturer </td>
          <td><p>BBA-DIU(Management), MBA -AIUB(Marketing) </p>
            <p>ACCA, UK </p></td>
        </tr>
        <tr>
          <td>Lia Akther </td>
          <td>Lecturer</td>
          <td>BBA, MBA-IIUC (Finance)</td>
        </tr>
        <tr>
          <td>Farhana Sharif </td>
          <td>Lecturer</td>
          <td>BBA, MBA-C.U(Management)</td>
        </tr>
        <tr>
          <td>Shirin Akther</td>
          <td>Lecturer</td>
          <td>BBA, MBA - C.U (Finance)</td>
        </tr>
        <tr>
          <td>Nur Jahan </td>
          <td>Lecturer</td>
          <td>BBA, MBA - C.U(Statisties)</td>
        </tr>
        <tr>
          <td>Sathi Barua </td>
          <td>Lecturer</td>
          <td>BBA, MBA - C.U(Marketing)</td>
        </tr>
        <tr>
          <td>Sadia Salam </td>
          <td>Lecturer</td>
          <td>BBA, MBA - D.U(Accounting Information System)</td>
        </tr>
        <tr>
          <td>Hadikatunuzum</td>
          <td>Lecturer</td>
          <td>BBA, MBA - SUST (Marketing) </td>
        </tr>
        <tr>
          <td>Munmun  </td>
          <td>Lecturer</td>
          <td>BBA, MBA - C.U(Marketing) </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p align="center" class="style3"><u>Adjunct Faculties of Business Administration</u></p>
    <p align="center" class="style3">&nbsp;</p>
    <table width="975" border="1" align="center">
      <tr>
        <td width="178"><div align="center" class="style9">Name</div></td>
        <td width="245"><div align="center" class="style8"><strong>Designation</strong></div></td>
        <td width="530"><div align="center" class="style8">Universities</div></td>
      </tr>
      <tr>
        <td>Prof. Dr. Harunur Rahid </td>
        <td>Professor</td>
        <td>Dept. of Accounting, C.U </td>
      </tr>
      <tr>
        <td><p>Prof. Dr. Tayeb chowdhury </p>        </td>
        <td>Professor</td>
        <td>Dept. of Marketing, C.U </td>
      </tr>
      <tr>
        <td><p>Prof. S.M. Salamutullah </p>
        <p>Bhuiyan</p></td>
        <td>Professor</td>
        <td>Dept. of Marketing, IIUC </td>
      </tr>
      <tr>
        <td>Prof. Md. aktaruzzaman </td>
        <td>Professor</td>
        <td>Dept. of Marketing</td>
      </tr>
      <tr>
        <td>Mir Md Moinul Islam  </td>
        <td>Senior Lecturer </td>
        <td>BBA, MBA - C.U </td>
      </tr>
      <tr>
        <td>Md. Awlad Hossain Sagar </td>
        <td>Lecturer</td>
        <td>C.U</td>
      </tr>
      <tr>
        <td>Md. Mustafa Kamal </td>
        <td>Lecturer</td>
        <td>C.U</td>
      </tr>
      <tr>
        <td>Assistant Prof. Ataur Rahman </td>
        <td>Assistant Professor </td>
        <td>C.U</td>
      </tr>
    </table>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p align="center" class="style10"><u>Faculties of LLB (Hons)</u>  </p>
    <p align="center" class="style10">&nbsp;</p>
    <div align="center">
      <table width="975" border="1">
        <tr>
          <td width="177"><div align="center" class="style8"><strong>Name</strong></div></td>
          <td width="250"><div align="center" class="style11">Degignation</div></td>
          <td width="526"><div align="center" class="style8"><strong>Universities</strong></div></td>
        </tr>
        <tr>
          <td height="35"><p>Adv. Md. Yousuf Alam  </p>
          </td>
          <td>Senior Lecturer </td>
          <td><p>LL.B (hon's), LLM (IIUC) </p>
            <p>M.Phil Research fellow (IU.k) </p></td>
        </tr>
        <tr>
          <td>Adv. Md. Anamul Hossain Chy </td>
          <td>Senior Lecturer </td>
          <td><p>LL.B (Hon's) LL.B (IIUC) </p>
          <p>M.phil  Research fellow (IU.k) </p></td>
        </tr>
        <tr>
          <td>ASM Noman </td>
          <td>Senior Lecturer </td>
          <td><p>LLB (Hon's) LL.M (Du)</p>
          <p>Judicial magistrate, Chittagong</p></td>
        </tr>
        <tr>
          <td>S M Masud Parvez </td>
          <td>Senior Lecturer </td>
          <td><p>LL.B (Hon's) LL.M (cu) </p>
          <p>Judicial magistrate, chittagong </p></td>
        </tr>
        <tr>
          <td>Md. Kamal Hossain </td>
          <td>Senior Lecturer </td>
          <td><p>LL.B (Hon's) LL.M (cu)</p>
            <p>Assistant Judge,  Judges court Chittagong  </p></td>
        </tr>
        <tr>
          <td>Adv. Md. Saidur Rahman </td>
          <td>Senior Lecturer </td>
          <td>LL.B (Hon's), LL.M (NU) </td>
        </tr>
        <tr>
          <td>Adv Md. Safiqul Islam (Liton) </td>
          <td>Senior Lecturer </td>
          <td>LL.B (Hon's) LL.M </td>
        </tr>
      </table>
    </div>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10"><u>Depatment of EEE</u> </p>
    <p align="center" class="style10">&nbsp;</p>
    <div align="center">
      <table width="975" border="1">
        <tr>
          <td width="182"><div align="center" class="style11">Name</div></td>
          <td width="248"><div align="center" class="style8"><strong>Designation</strong></div></td>
          <td width="523"><div align="center" class="style11">Universities</div></td>
        </tr>
        <tr>
          <td>Mohammed Mizanur Rahman Chowdhury </td>
          <td>Lecturer</td>
          <td>IIUC</td>
        </tr>
        <tr>
          <td>Mohammed Ifthakhar Haider </td>
          <td>Lecturer</td>
          <td>BUET</td>
        </tr>
        <tr>
          <td>Habib Mohammed Ullah </td>
          <td>Lecturer</td>
          <td>BUET</td>
        </tr>
        <tr>
          <td>Mohammed Imrul Islam </td>
          <td>Lecturer</td>
          <td>BUET</td>
        </tr>
        <tr>
          <td>S.M.Asif Bin Yousuf </td>
          <td>Lecturer</td>
          <td>Anna University, Chennai</td>
        </tr>
        <tr>
          <td>mohammed samrat </td>
          <td>Lecturer</td>
          <td>IIUC</td>
        </tr>
        <tr>
          <td>Dr.Idris Miah </td>
          <td>Professor</td>
          <td>C.U - EEE </td>
        </tr>
        <tr>
          <td>Md. Abdullah Hil Yakib </td>
          <td>Lecturer</td>
          <td>SUST</td>
        </tr>
        <tr>
          <td>Shana Akter </td>
          <td>Lecturer</td>
          <td>AIUB</td>
        </tr>
        <tr>
          <td>Tabassum Islam </td>
          <td>Lecturer</td>
          <td>CUET</td>
        </tr>
		
            <td>Mohammed Rafiqul Islam </td>
              <td>Lecturer</td>
          <td>CUET</td>
        </tr>
      </table>
    </div>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10"><u>Department of English</u></p>
    <p align="center" class="style10">&nbsp;</p>
    <div align="center">
      <table width="975" border="1">
        <tr>
          <td width="182"><div align="center" class="style11">Name</div></td>
          <td width="248"><div align="center" class="style8"><strong>Designation</strong></div></td>
          <td width="523"> <div align="center"><span class="style8"><strong>Universities</strong> </span></div></td>
        </tr>
        <tr>
          <td>Prof. Durdaua Matin </td>
          <td>Professor</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Prof. Azmeri Ara begum </td>
          <td>Professor</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Prof. Minul Hasan </td>
          <td>Professor</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Asst. Prof. Rukan Uddin </td>
          <td>Asst. Professor</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Arman Hossain </td>
          <td>Lecturer</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Nahid Akther </td>
          <td>Lecturer</td>
          <td>C.U</td>
        </tr>
        <tr>
          <td>Shahanz Akther </td>
          <td>Lecturer</td>
          <td>IIUC</td>
        </tr>
        <tr>
          <td>Jowel Chowdhury </td>
          <td>Senior Lecturer</td>
          <td>C.U</td>
        </tr>
      </table>
    </div>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10">&nbsp;</p>
    <p align="center" class="style10">&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;  </p>
<div id="outer_footer">
   <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

  </div>
</body>
</html>