
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Registration Information</title>
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>
<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>     <!--Menu Area -->
    <div id="nav">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   


    </div>    <!--content area -->
    <div id="content">
      <!--banner section -->
      <div id="banner_wrapp_inner">
        <div id="banner_inner"> <img src="images/banner-contact.jpg" alt="" /> </div>
      </div>
      <div id="contact_us">
        <h1>&nbsp;</h1>
        <div class="addressbox">
          <div class="postal_address">
            <h2>Registration Information</h2>
            <ul>
              <li>
                <div class="desc1">
<b>Admission policy</b><br /><br />

<p align="justify">We assess each applicant as an individual, by evaluating work experience, academic profile, professional and personal maturity, and patterns of success. The Admission Committee looks for individuals who will be future leaders in respective fields. Southern University is interested in students who want to learn. The university does not discriminate any individual because of age, sex, race, marital status, physical disability or religious affiliations.The office of admission reserves the right to request any additional information before a decision for admission is made. Providing false or incomplete information on the application for admission may result in a denial of admission or revocation of enrollment.</p><br />

<b>Admission Requirements</b><br /><br />

Acceptable scores at Southern Admission Test (SAT) or at least 550 in (TOEFL).<br /><br />

<p align="justify">New students are admitted throughout the year, for spring, summer and fall semesters. Please check for the last date of submitting application in a particular semester. All admission queries should be directed to the Admission. Application forms are available @ Tk. 400/=Only completed applications with all certificates, transcripts, mark sheets, letters of recommendation and photographs, etc. of the students will be processed.</p><br />

<b>Admission: Undergraduate</b><br /><br />

<p align="justify">Minimum 12 (Twelve) years of schooling prior to undergraduate program or equivalents. At least a second division (or 2.5/5.0 GPA) in the examinations.</p><br />

<b>Admission Graduate</b><br /><br />

To enter into the Masters Program, one must have the following:<br /><br />
<p align="justify">Minimum 14 (Fourteen) years of schooling prior to Masters. OR 4 (Four) years Bachelor Degree (or 3 years Bachelor plus 1 year Master degree).</p><br />

Relative 3-5 years working experience is to be considered as an added advantage to enroll into Graduate Program.<br /><br />

<center><b>"Only self-motivated persons are encouraged to apply in Executive Program"</b></center><br /><br />

<b>Admission Diploma /Certificate</b><br /><br />

<p align="justify">There are no fixed criteria in these programs. The Admission Committee looks for individuals who want to learn in any suitable course(s). Students who want to learn are encouraged to enroll.</p><br />

<b>Course Enrollment Dates</b><br /><br />

<p align="justify">Students approved for admission may enroll in on-campus courses any time prior to and including the first two weeks of class for each semester.</p><br />

<b>Credit Evaluations</b><br /><br />

<p align="justify">Official evaluation of transferred credit is made as quickly as possible after you are admitted to a program. The Admissions Office will help you with an unofficial transcript review at your request. If a course taken at another institution is not offered at SOUTHERN, elective credit may be granted for that course. Elective credits may be applied toward degree requirements but may not be used to satisfy any specific course requirement. Grades less than C- will not transfer. The Registrar??s Office completes transferred credit evaluations. The application for course exemption and credit transfer by a newly enrolled student in to a program may be submitted to the head of concern department and decision of the dean will be final.</p><br />

<b>Early Admission Policy</b><br /><br />

<p align="justify">Students under the age of 18 may apply for early admission to Southern, who appeared Higher Secondary Certificate (HSC) board exam or A level exam or any equivalent exam and yet to get passing grade can be qualified for provisional admission. Student must submit recommendation from proper university authority with proper documents and must fulfill admission criteria within one year. Early applicants to Southern must provide an official transcript of all high school work and test scores from Southern Admission Test (SAT). The results of these tests, along with the high school record, will be used to determine the applicant??s eligibility for admission.</p><br />

<b>Entrance Examinations</b><br /><br />

<p align="justify">All incoming freshmen are required to take admission/evaluation test. The results are helpful in counseling and placement. An applicant who does not hold a high school diploma may be admitted if s/he is qualified through an interview with the admission committee and presents all required documents.</p><br />

<b>English Proficiency</b><br /><br />

All new applicants to the program are required to present the evidence of proficiency in English language prior to admission.<br /><br />

<b>Former Students</b><br /><br />

<p align="justify">Former full-time Southern University students, who miss one or more semesters (not including summer) and attend another college or university, must apply for readmission before the semester commencement (of re-entry). There is no application fee. Student must submit official transcripts from any institution attended since leaving Southern and meet our transfer student admission policy.</p><br />

<b>Guest Students</b><br /><br />

<p align="justify">Students enrolled at another college or university may be admitted to Southern for one semester as a guest student. An extension of one additional semester may be granted for extenuating circumstances. If student intend to enroll full-time for more than one semester, student must submit an application for admission as a transferred student. Guest students assume responsibility for determining if Southern courses apply to their program at the college/University from which they intend to graduate.</p><br />

<b>International Admission</b><br /><br />

<p align="justify">All international students must submit a certified letter of support from the sponsors. Students must submit application two months before the semester begins.</p><br />

<b>Readmission</b><br /><br />

<p align="justify">Students seeking readmission must complete an application for admission if more than two semesters have elapsed since their last attendance at the University. Such students must comply with the government formalities for admission.</p><br />

<b>Transfer for Degree</b><br /><br />

<p align="justify">Transfer students seeking admission to University must have a minimum 2.5 grade point average from recognized institution of higher education and must have been in good standing from the institution last attended. In addition, applicants must fulfill and submit the admission requirements of University. All transcripts become the property of Southern University and are not returnable. Transferred students who were denied before admission may re-apply after taking additional courses that raise their overall GPA to above a 2.5.</p><br />
 
<b>Policy: Tuition/Fees</b><br /><br />

<p align="justify">All tuition and fees are payable according to established due dates. Students delinquent in payment of a financial obligation are subject to enrollment cancellation and/or late fees until all amounts due the University are paid or satisfactory arrangements are made with the Business Office. Anyone who is delinquent in any obligation to the University will not be allowed to register for classes. Additionally, University services will not be provided until financial obligations are met. Registration is not complete until fees are paid. A check or draft returned to the University and not honored by the bank constitutes nonpayment and results in cancellation of registration.</p><br />

<b>Withdrawal/Refunds</b><br /><br />

If you decide to drop your classes, you must complete each step to officially withdraw from the University.<br /><br />

1. Pick up an Official Withdrawal Request from the Student Affairs/Admission<br />
2. Clear any outstanding charges or holds that may prevent your return at a later date or prevent the release of your academic record.<br />
3. Complete the entire Withdrawal Form and obtain the required signatures (shown on the form).<br />
4. If you have benefited from financial aid/scholarship, you may be required to visit account/Finance Office.<br />
<p align="justify">5. Deliver the completed form to the Accounts Office. Your withdrawal date will be determined by the date the completed form is submitted to the Registrars Office. Any refunds will be calculated as of that date. All withdrawals should be done in person. If you are unable to complete the process in person, the registrar is the only University authority who can authorize the process of your withdrawal over the phone. Refunds are made in accordance with the above schedule. After your completed withdrawal form is accepted, your University charges will be reduced according to the schedule. If you have not received any form of financial aid/scholership and there is a credit balance on your account, you will be sent a refund check. If you have received aid, your aid may have to be returned to the appropriate source. You may owe money!</p><br />

<b>Withdrawal and Refund Policy</b><br /><br />

<b><i>Courses Dropped Time of Withdrawal % of Refund</i></b><br /><br />

Any or all classes Prior to class - 6th school day* 100%<br /><br />
Dropping all classes 7th-8th school day 90%<br />
Dropping all classes 9th-19th school day 50%<br />
Dropping all classes 20th-38th school day 25%<br /><br />
*There are no refunds for partial drops after the sixth day.<br />
                </div>
              </li>
            </ul>
          </div>
  
        </div>
           <br class="clear" />
      </div>
</div>
      <br class="clear" />
    </div>
  </div>
  <!--footer -->
  <div id="outer_footer">
     <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div></body>
</html>
