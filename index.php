
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Southern University Bangladesh :: Home</title>
<link rel="shortcut icon" href="images/favicon.ico">
<!--CSS -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<!-- Stylesheet -->
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.4.css" media="screen" />
<!--Js -->
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/cufon-yui.js"></script>
<script type="text/javascript" src="js/swiss.js"></script>
<script type="text/javascript" src="js/hoverIntent.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<link href="2/js-image-slider.css" rel="stylesheet" type="text/css" />
<script src="2/js-image-slider.js" type="text/javascript"></script>
<!-- Javascript -->
<script src="js/jquery.min.js" type="text/javascript"></script>
<script src="js/ddsmoothmenu.js" type="text/javascript"></script>
<script src="js/contentslider.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jcarousellite_1.0.1.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.1.js"></script>
<script type="text/javascript" src="js/DIN_500.font.js"></script>
<script type="text/javascript" src="js/menu.js"></script>
<script type="text/javascript" src="js/tabs.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.4.pack.js"></script>


<body>
<!--wrapper -->
<div id="outer_wrapper">
  <div id="wrapper">
    <!--header -->
    <div id="header"> <a href="index.php"><img src="images/logo.png" alt="" id="logo" /></a>
      <div id="right_header">
         <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
<div id="top_nav">
          <ul>
            <li><a href="contact.php"><span>Contact us</span></a></li>
            <li><a href="#"><span>Career</span></a></li>
            <li><a href="#"><span>Student Portal</span></a></li>          
            <li><a href="#" target="_blank"><span>Webmail</span></a></li>
            <li><a href="#" ><span>News</span></a></li>
            
          </ul>
          
        </div>
        <div id="search_header">
          <ul>
            <li></li>
            <li></li>
          </ul>
        </div>
    
                    
      </div>
    </div>    <!--Menu Area -->
    <div id="nav">
            <link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" type="text/css" href="css/ddsmoothmenu.css" />
 <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="about.php">About Us</a>
          <ul>
            <li><a href="history.php">History & Mission</a></li>
            <li><a href="accreditation.php">Accreditation</a></li>
            <li><a href="messagevc.php">Message - VC</a></li>
            <li><a href="messagefounder.php">Message - Founder</a></li>
            <li><a href="membertrustees.php">Member of Trustee</a></li>
          </ul>
        </li>
        <li><a href="#">Academics</a>
          <ul>
            <li><a href="faculties.php">Faculty Members</a></li>
            
            <li><a href="#">Departments</a>
           		<ul>
                <li><a href="bus.php" target="_blank">Business</a></li>
             <li><a href="fse.php" target="_blank">Computer Science</a></li>
                <li><a href="law.php" target="_blank">Law</a></li>             
                 <li><a href="english.php" target="_blank">English</a></li>
           <li><a href="fse.php" target="_blank">ECE & EEE</a></li>
                <li><a href="pharm.php" target="_blank">Pharmacy</a></li>
                 <li><a href="civileng.php" target="_blank">Civil Engineering</a></li>
               <li><a href="islamic.php" target="_blank">Islamic Studies</a></li>  
             	</ul>  
			</li>                          
          </ul>
        </li>        
        <li><a href="#">Admission</a>
          <ul>
            <li><a href="requirements.php">Requirements</a></li>
            <li><a href="information.php">Information</a></li>
            <li><a href="fees.php">Tuition & Other Fees</a></li>
            <li><a href="financialassist.php">Financial Assistance</a></li>
            <li><a href="credittrans.php">Credit Transfer</a></li>
            <li><a href="images/admit.pdf" target="_blank">Download Form</a></li>            
          </ul>
        </li>
        <li><a href="faq.php">Faq</a></li> 
        <li><a href="apply.php">Apply Online</a></li> 
                <li><a href="gallery/gallery.php" target="_blank">Gallery</a></li>
                   
                    


    </div>      <!--banner section -->
    <div class="container2"><a href="stdverify.php" title="Verification for All Students"><font color="#FFFFFF">Student Verification</font></a> </div>
      <div id="banner_wrapp">
                   	<!-- Banner -->
         <div id="sliderFrame">
        <div id="slider">
              
        <img src="pic/1.jpg" alt="Convocation 2010" />
      <a href="#"> <img src="pic/2.jpg" alt="Convocation 2010" /></a>
                   
            
           </div>
        <!--thumbnails-->
        <div id="thumbs">
            <div class="thumb">
                <div class="frame"><a href="images/future campus.jpg"><img src="images/future campus.jpg" /></a></div>
                <div class="thumb-content"><p><a href="#">Southern Future Campus</p></a></div>
                <div style="clear:both;"></div>
            </div>
            <div class="thumb">
                <div class="frame"><img src="images/admission.png" /></div>
                <div class="thumb-content"><p><a href="#" target="_blank">Admission Open</p>Summer 14</a></div>
                <div style="clear:both;"></div>
            </div>
            <div class="thumb">
                <div class="frame"><img src="images/banner3.jpg" /></div>
                <div class="thumb-content"><p>Southern Library</p>E library Coming soon</div>
                <div style="clear:both;"></div>
            </div>
            <div class="thumb">
                <div class="frame"><img src="images/banner.jpg" /></div>
                <div class="thumb-content"><p><a href="#">A Complete Web Based University</a></p>
                </div>
                <div style="clear:both;"></div>
            </div>
        </div>
        <!--clear above float:left elements-->
        <div style="clear:both;height:0;"></div>
    </div>
      </div>
      <div class="study_programs">
        <ul>
          <li><a href="#"><img src="images/img-md1.jpg" alt="" /></a>
            <div class="programdetail">
              <div class="headingprogram">
                <h2>Schools & Departments</h2>
              </div>
              <div class="detailprogram">
                <p>Business Administration | Science & Engineering | Arts, Law & Social Science..</p>
                <div class="readmore"> <a href="#"></a> </div>
              </div>
            </div>
          </li>
          <li><a href="#"><img src="images/img-md2.jpg" alt="" /></a>
              <div class="programdetail">
                <div class="headingprogram">
                  <h2>Courses Offered</h2>
                </div>
                <div class="detailprogram">
                  <p>We offer a wide range of courses.</p>
                  <div class="readmore"> <a href="#"> Read More</a> </div>
                </div>
              </div>
          </li>
          <li><a href="#"><img src="images/img-md3.jpg" alt="" /></a>
            <div class="programdetail">
              <div class="headingprogram">
                <h2>Academic Facilities</h2>
              </div>
              <div class="detailprogram">
                <p>Computer Lab, Library, Cafeteria and many more..</p>
                <div class="readmore"> <a href="#"> Read More</a> </div>
              </div>
            </div>
          </li>

            <FONT SIZE="3" FACE="courier" COLOR=white><MARQUEE  BEHAVIOR=right BGColor=blue scrolldelay="300">Our new ""Admission & Information Campus"", Khulna-7/1. Jalil Shoroni, Bayra, Khulna.</MARQUEE></FONT>
        </ul>
</div>
      <!--News and spotlight container -->
      <div class="outer_row">
        <div class="col-left">
          <div class="spotlight_slider">
            <h2>Notices/<a href="#" target="_blank">Archive</a>            </h2>
            <div class="box_spotlight">
              <p align="justify"><img src="images/img-sm1.jpg" alt="" /></a><strong>Recent Notices  : </strong></p>
              <p align="justify">Ramadan Class Schedule Summer 14 <strong><a href="#" target="_blank"><font color="#FF0000">[E-MBA]</font></a>- <a href="#"><font color="#FF0000">[E-BBA]</a></font></strong></p>
              <p align="justify"><strong>Mid-Term Exam Routine (Summer 2014)</strong></p>
              <p align="justify"><strong><a href="#"><font color="#FF0000">CSIT, ECE &amp; EEE</font></a></strong> <strong>| <a href="#"><font color="#FF0000">Business</font></a></strong></p>
</div>
            <div class="box_spotlight">
              <p align="justify"><img src="images/img-sm1.jpg" alt="" /></a><strong>Class Routine  -Summer 2014: </strong></p>
<p align="justify"><a href="#">BBA </a>|| <a href="#" target="_blank">Evening MBA </a>|| <a href="#">Evening BBA</a>||<a href="#"> Friday MBA</a><br />
          </p>
<p align="justify"><a href="#" target="_blank"><strong>LLB (Hons)</strong></a> | | <a href="#"><strong>LLB (2 Years)</strong></a> | <a href="#"><strong>CSIT, ECE , EEE</strong></a></p>
</div>
            <div class="box_spotlight">
              
             
</div>
          </div>
          <div class="news_slider">
            <h2>Events/<a href="#" target="_blank">Archive</font></a></h2>
            <p><a href="#"><img src="images/Logo-for-web.jpg" alt="" width="209" height="83" /></a></p>
           <marquee direction=up> <div class="news_box"> 
            <p><img src="images/img-sm1.jpg" alt="" />
          <a href="#" target="_blank">Admission Going On-2014</a><br />
          </p> 
              <div class="news_desc">
                   
  </div>
          </div></marquee>
            <div class="news_box">
              <div class="news_desc"></div>
            </div>
            
            <div class="news_box">
              <div class="news_desc"></div>
            </div>
          </div>
        </div>
           <script type="text/javascript">
         function go()
   {
     window.location=document.getElementById("link").value
   }
       </script>
  <center><form>
    <p><b><font color="#000099" size="+1">Course Finder</font></b>
    </h2>
    <p>
      <select name="link" id="link" onchange="go()">
        <option value="#">Choose your Department</option>
        <option value="bus.php">BUSINESS</option>
                <option value="fse.php">Computer Science</option>
        <option value="law.php">LAW</option>
        <option value="english.php">ENGLISH</option>
          <option value="fse.php">ECE & EEE</option>
              <option value="pharm.php">PHARMACY</option>
        <option value="civileng.php">CIVIL ENGINEERING</option>
        <option value="islamic.php">ISLAMIC STUDIES(Religion)</option>
      </select>
</p>
  </form>
  </center> 
        <div class="col-right">
          <div class="newsletter"><a href="#" target="_blank"><img src="images/bg-newsletter.jpg" /></a></div>
                      <a href="stdverify.php" target="_blank"><div class="coursesearch">

            <h2 align="center"><strong><i><font size="+2" color="#00FF00"><blink>Student Verification</blink></font></i></strong></h2>
          </div></a>
          <div class="coursesearch">
<center>
  <a href="#" target="_blank"><img src="images/radio.png" /></a>
</center>
            <a href="#" target="_blank"><img src="images/bba.jpg" alt="" width="310" height="51" /></a>
            <a href="#" target="_blank"><img src="images/llb-1.jpg" alt="" width="311" height="37" /></a>
            
          </div>          
        </div>
      </div>
      <br class="clear" />
    </div>
  </div>
  <ul>
  <!--footer -->
  <div id="outer_footer">
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/colorbox.css" rel="stylesheet" type="text/css" />

</head>

<body>
<div id="footer">
      <div id="left_footer">
        <ul>
          <li>
            <h2>About Us</h2>
            <ul>
              <li><a href="index.php">Home</a></li>
              <li><a href="about.php">About SUB</a></li>
              <li><a href="contact.php">Contact Us</a></li>
            </ul>
          </li>
          <li>
            <h2>Academia</h2>
            <ul>
              <li><a href="images/calender.jpg">Academic Calendar</a></li>
              <li><a href="#">Admissions 2014</a></li>
              <li><a href="faculties.php">Faculty Members</a></li>            
            </ul>
          </li>
          <li>
            <h2>Quick Links</h2>
            <ul>
              <li><a href="#">Student Portal</a></li>
              <li><a href="#">Webmail</a></li>
                      <li><a href="#">Career Development</a></li>  

            </ul>
          </li>
        </ul>
      </div>
      <div id="right_footer">
        <div class="tweetbox">
          <div class="left_tweet">
            <p><b>Southern University Bangladesh</b></p>
            <p><i> Committed to Academic Excellence  </i></p>
             </div>
          <div class="right_tweet"> <img src="images/bg-tweet.png" alt="" />
            <h3><a href="#" target="_blank">Info</a></h3>
          </div>
        </div>
        <div class="bottom_links">
          <div class="left_links">
            <ul>
              <li>
                <h3>Follow Us</h3>
              </li>
              <li><a href="#" target="_blank"><img src="images/img-fb.png" alt="" /></a></li>
              <li><a href="#" target="_blank"><img src="images/img-tw.png" alt="" /></a></li>
            </ul>
          </div>
          <div class="right_links">
            <h3>Call:01881634452</h3>
          </div>
        </div>
      </div>
      <div class="bottom_footer">
        <p>CopyRight By &copy; Southern University Bangladesh-2014. All Rights Reserved -</br><b><a href=#>Developed by DreamStar Group </a></b></p>
        <a href="#" id="topScroll">Back to Top</a> </div>
    </div>
  </div>
  <br class="clear" />
</body>
</html>

</div>

</body>
</html>